﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using FXClient.Core.SyncNet;
using System;
using System.Threading.Tasks;

namespace Chat
{
    public class Init : BaseScript
    {
        bool isBlock = true;
        public Init()
        {
            Tick += Update;
            Function.Call(Hash.REGISTER_NUI_CALLBACK_TYPE, "loaded");
            EventHandlers["__cfx_nui:loaded"] += new Action(() =>
            {
                isBlock = false;
                Function.Call(Hash.SET_TEXT_CHAT_ENABLED, false);
                Network.Server("chat:init");
            });

            Function.Call(Hash.REGISTER_NUI_CALLBACK_TYPE, "chatResult");
            EventHandlers["__cfx_nui:chatResult"] += new Action<dynamic>((data) =>
            {
                if (isBlock) return;
                chatInputActive = false;
                Function.Call(Hash.SET_NUI_FOCUS, false, false);
                string message = "";
                try
                {
                    if ((message = data.message) != null) Network.Server("chat:getMessage", message);
                }
                catch { Network.Server("chat:cancelMessage", message); }
            });

            Network.onServerData += new Network.onServerDataDelegate((trigger, data) => { if (trigger == "chat:message") Function.Call(Hash.SEND_NUI_MESSAGE, "{ \"type\" : \"ON_MESSAGE\", \"message\" : \"" + data + "\" }"); });
            Network.onServerData += new Network.onServerDataDelegate((trigger, data) => { if (trigger == "chat:clear") Function.Call(Hash.SEND_NUI_MESSAGE, "{ \"type\" : \"ON_CLEAR\" }"); });
        }

        bool chatInputActive = false;
        private Task Update()
        {
            if (!chatInputActive && Game.IsControlJustReleased(0, Control.MpTextChatAll) && !isBlock)
            {
                chatInputActive = true;
                Function.Call(Hash.SET_NUI_FOCUS, true, false);
                Function.Call(Hash.SEND_NUI_MESSAGE, "{ \"type\" : \"ON_OPEN\" }");
            }

            return Task.FromResult(true);
        }
    }
}
