﻿using LibSharp.CSharpCore;
using System.Collections.Generic;

namespace CitizenFX.Core
{
    public class _VehicleWheelCollection
    {
        VehicleCore _owner;
        readonly Dictionary<int, _VehicleWheel> _vehicleWheels = new Dictionary<int, _VehicleWheel>();
        public _VehicleWheelCollection(VehicleCore owner) =>  _owner = owner;
        public _VehicleWheel this[int index]
        {
            get
            {
                _VehicleWheel vehicleWheel = null;
                if (!_vehicleWheels.TryGetValue(index, out vehicleWheel))
                {
                    vehicleWheel = new _VehicleWheel(_owner, index);
                    _vehicleWheels.Add(index, vehicleWheel);
                }
                return vehicleWheel;
            }
        }
    }
}