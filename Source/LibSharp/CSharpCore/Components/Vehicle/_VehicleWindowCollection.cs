﻿using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using System.Collections.Generic;

namespace CitizenFX.Core
{
    public class _VehicleWindowCollection
    {
        VehicleCore _owner;
        readonly Dictionary<VehicleWindowIndex, _VehicleWindow> _vehicleWindows = new Dictionary<VehicleWindowIndex, _VehicleWindow>();
        internal _VehicleWindowCollection(VehicleCore owner) =>_owner = owner;
        public _VehicleWindow this[VehicleWindowIndex index]
        {
            get
            {
                _VehicleWindow vehicleWindow = null;
                if (!_vehicleWindows.TryGetValue(index, out vehicleWindow))
                {
                    vehicleWindow = new _VehicleWindow(_owner, index);
                    _vehicleWindows.Add(index, vehicleWindow);
                }
                return vehicleWindow;
            }
        }
        public bool AreAllWindowsIntact => API.AreAllVehicleWindowsIntact(_owner.Handle);
        public void RollDownAllWindows() => API.RollDownWindows(_owner.Handle);
    }
}