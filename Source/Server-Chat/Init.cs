﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Chat
{
    public class Init : BaseScript
    {
        public Init()
        {
            EventHandlers["chat:messageCommand"] += new Action<Player, string, string[]>(onCommand);
            EventHandlers["chat:messageText"] += new Action<Player, string>(onMessage);
        }

        private void onMessage([FromSource] Player player, string message)
        {
            Debug.Write($"[LOG][CHAT] {player.Name}: {message}\n");
            MessageToAll($"{player.Name}: {message}");
        }
        private void onCommand([FromSource] Player player, string commandName, string[] args)
        {
            Debug.Write($"[LOG][CHAT-CMD] {player.Name}: #{commandName} {print(args)}\n");
            MessageToPlayer(player, $"CMD {commandName}");
        }


        private string print(string[] args)
        {
            string _return = "";
            foreach (var raw in args)
            {
                _return += $" {raw}";
            }
            return _return;
        }

        public void MessageToAll(string message) => TriggerClientEvent("chat:message", message);
        public void MessageToPlayer(Player player, string message) => TriggerClientEvent(player, "chat:message", message);
    }
}
