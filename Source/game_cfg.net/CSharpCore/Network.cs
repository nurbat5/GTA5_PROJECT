﻿using CitizenFX.Core;
using System;

namespace LibSharp.CSharpCore
{
    public class Network : BaseScript
    {
#if !FX_SERVER
        public Network()
        {
            //Trigger
            EventHandlers["FX:CLIENT:TRIGGER"] += new Action<int, string>((sender, trigger) => onNetworkTrigger(Players[sender], trigger));
            EventHandlers["FX:CLIENT:SERVER:TRIGGER"] += new Action<string>(trigger => onServerTriggerPrivate(trigger));

            //Data
            EventHandlers["FX:CLIENT:DATA"] += new Action<int, string, dynamic>((sender, trigger, data) => onNetworkData(Players[sender], trigger, data));
            EventHandlers["FX:CLIENT:SERVER:DATA"] += new Action<string, dynamic>((trigger, data) => onServerData(trigger, data));

            //Local
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>(trigger => onLocalTrigger(trigger));
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>((trigger, data) => onLocalData(trigger, data));
        }

        //LOCAL TRIGGER
        public delegate void LocalTriggerDelegate(string triggerName);
        static public event LocalTriggerDelegate onLocalTrigger = new LocalTriggerDelegate(new LocalTriggerDelegate((trigger) => { }));

        //LOCAL DATA
        public delegate void LocalDataDelegate(string triggerName, dynamic data);
        static public event LocalDataDelegate onLocalData = new LocalDataDelegate(new LocalDataDelegate((trigger, data) => { }));

        //TRIGGER
        public delegate void NetworkTriggerDelegate(Player sender, string triggerName);
        static public event NetworkTriggerDelegate onNetworkTrigger = new NetworkTriggerDelegate(new NetworkTriggerDelegate((sender, trigger) => { }));

        //TRIGGER SERVER
        public delegate void ServerTriggerPrivateDelegate(string triggerName);
        static public event ServerTriggerPrivateDelegate onServerTriggerPrivate = new ServerTriggerPrivateDelegate(new ServerTriggerPrivateDelegate((trigger) => { }));

        //DATA
        public delegate void NetworkDataDelegate(Player sender, string triggerName, dynamic data);
        static public event NetworkDataDelegate onNetworkData = new NetworkDataDelegate(new NetworkDataDelegate((sender, trigger, data) => { }));

        //DATA SERVER
        public delegate void ServerDataDelegate(string triggerName, dynamic data);
        static public event ServerDataDelegate onServerData = new ServerDataDelegate(new ServerDataDelegate((trigger, data) => { }));

        //== [ CMD ] ==

        //LOCAL CMD
        static public void Local(string triggerName)
        {
            TriggerEvent("FX:LOCAL:TRIGGER", triggerName);
            Debug.WriteLine("[LOCAL][SEND] TRIGGER: {0}", triggerName);
        }
        static public void Local<T>(string triggerName, T data)
        {
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
            Debug.WriteLine("[LOCAL][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }
        static public void Local<T>(string triggerName, params T[] data)
        {
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
            Debug.WriteLine("[LOCAL][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }

        //SERVER CMD
        static public void Server(string triggerName)
        {
            TriggerServerEvent("FX:SERVER:CLIENT:TRIGGER", triggerName);
            Debug.WriteLine("[SERVER][SEND] TRIGGER: {0}", triggerName);
        }
        static public void Server<T>(string triggerName, T data)
        {
            TriggerServerEvent("FX:SERVER:CLIENT:DATA", triggerName, data);
            Debug.WriteLine("[SERVER][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }
        static public void Server<T>(string triggerName, params T[] data) => Local(triggerName, data);

        //TRIGGER
        static public void Send(Player player, string triggerName)
        {
            TriggerServerEvent("FX:SERVER:TRIGGER", triggerName, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FOR: {1}", triggerName, player.Name);
        }
        static public void Send(string triggerName) => Send(Game.Player, triggerName);

        //PACKET
        static public void Send(Player player, Action data)
        {
            TriggerServerEvent("FX:SERVER:PACKET", data, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] PACKET: {0}, FOR: {1}", data, player.Name);
        }
        static public void Send(Action data) => Send(Game.Player, data);

        //DATA
        static public void Send<T>(Player player, string triggerName, T data)
        {
            TriggerServerEvent("FX:SERVER:DATA", triggerName, data, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FOR: {3}", triggerName, data.GetType().Name, data, player.Name);
        }
        static public void Send<T>(string triggerName, T data) => Send(Game.Player, triggerName, data);
        static public void Send<T>(string triggerName, params T[] data) => Send(triggerName, data);
        static public void Send<T>(Player player, string triggerName, params T[] data) => Send(player, triggerName, data);
#else
        public Network()
        {
            EventHandlers["FX:SERVER:CLIENT:TRIGGER"] += new Action<Player, string>(FX_CLIENT_TRIGGER);
            EventHandlers["FX:SERVER:CLIENT:DATA"] += new Action<Player, string, dynamic>(FX_CLIENT_DATA);

            EventHandlers["FX:SERVER:TRIGGER"] += new Action<Player, string, int>(FX_SERVER_TRIGGER);
            EventHandlers["FX:SERVER:DATA"] += new Action<Player, string, dynamic, int>(FX_SERVER_DATA);
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>(FX_LOCAL_TRIGGER);
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>(FX_LOCAL_DATA);
        }

        //TRIGGER LOCAL
        private void FX_LOCAL_TRIGGER(string triggerName) => onLocalTrigger(triggerName);
        public delegate void onLocalTriggerDelegate(string triggerName);
        private static void TRIGGER_LOCAL(string triggerName) { }
        public static event onLocalTriggerDelegate onLocalTrigger = new onLocalTriggerDelegate(TRIGGER_LOCAL);

        //DATA LOCAL
        private void FX_LOCAL_DATA(string triggerName, dynamic data) => onLocalData(triggerName, data);
        public delegate void onLocalDataDelegate(string triggerName, dynamic data);
        private static void DATA_LOCAL(string triggerName, dynamic data) { }
        public static event onLocalDataDelegate onLocalData = new onLocalDataDelegate(DATA_LOCAL);

        //TRIGGER SERVER
        private void FX_CLIENT_TRIGGER([FromSource] Player fromPlayer, string triggerName) => onNetworkTriggerServer(fromPlayer, triggerName);
        public delegate void onNetworkTriggerServerDelegate(Player fromPlayer, string triggerName);
        private static void TRIGGER_CLIENT(Player fromPlayer, string triggerName) { }
        public static event onNetworkTriggerServerDelegate onNetworkTriggerServer = new onNetworkTriggerServerDelegate(TRIGGER_CLIENT);

        //DATA SERVER
        private void FX_CLIENT_DATA([FromSource] Player fromPlayer, string triggerName, dynamic data) => onNetworkDataServer(fromPlayer, triggerName, data);
        public delegate void onNetworkDataServerDelegate(Player fromPlayer, string triggerName, dynamic data);
        private static void DATA_CLIENT(Player fromPlayer, string triggerName, dynamic data) { }
        public static event onNetworkDataServerDelegate onNetworkDataServer = new onNetworkDataServerDelegate(DATA_CLIENT);

        //TRIGGER
        private void FX_SERVER_TRIGGER([FromSource] Player fromPlayer, string triggerName, int toPlayer) => onNetworkTrigger(fromPlayer, Players[toPlayer], triggerName);
        private static void TRIGGER_SYNC(Player client, Player toPlayer, string triggerName) { }
        public delegate void onNetworkTriggerDelegate(Player client, Player toPlayer, string triggerName);
        public static event onNetworkTriggerDelegate onNetworkTrigger = new onNetworkTriggerDelegate(TRIGGER_SYNC);

        //DATA
        private void FX_SERVER_DATA([FromSource] Player fromPlayer, string triggerName, dynamic data, int toPlayer) => onNetworkData(fromPlayer, Players[toPlayer], triggerName, data);
        private static void DATA_SYNC(Player client, Player toPlayer, string triggerName, dynamic data) { }
        public delegate void onNetworkDataDelegate(Player client, Player toPlayer, string triggerName, dynamic data);
        public static event onNetworkDataDelegate onNetworkData = new onNetworkDataDelegate(DATA_SYNC);

        //CMD
        static public void Local(string triggerName) => TriggerEvent("FX:LOCAL:TRIGGER", triggerName);
        static public void Local<T>(string triggerName, T data) => TriggerEvent("FX:LOCAL:TRIGGER", triggerName, data);
        static public void Local<T>(string triggerName, params T[] data) => TriggerEvent("FX:LOCAL:TRIGGER", triggerName, data);

        static public void Send(string triggerName)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FROM: SERVER", triggerName);
            TriggerClientEvent("FX:CLIENT:SERVER:TRIGGER", triggerName);
        }
        static public void Send(Player client, string triggerName)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FOR: {1}, FROM: SERVER", triggerName, client.Name);
            TriggerClientEvent(client, "FX:CLIENT:SERVER:TRIGGER", triggerName);
        }
        static public void Send<T>(string triggerName, T data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FROM: SERVER", triggerName, data.GetType(), data);
            TriggerClientEvent("FX:CLIENT:SERVER:DATA", triggerName, data);
        }
        static public void Send<T>(Player client, string triggerName, T data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FOR: {3}, FROM: SERVER", triggerName, data.GetType(), data, client.Name);
            TriggerClientEvent(client, "FX:CLIENT:SERVER:DATA", triggerName, data);
        }
        static public void Send<T>(string triggerName, params T[] data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FROM: SERVER", triggerName, data.GetType(), data);
            TriggerClientEvent("FX:CLIENT:SERVER:DATA", triggerName, data);
        }
        static public void Send<T>(Player client, string triggerName, params T[] data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FOR: {3}, FROM: SERVER", triggerName, data.GetType(), data, client.Name);
            TriggerClientEvent(client, "FX:CLIENT:SERVER:DATA", triggerName, data);
        }

#endif

    }
}
