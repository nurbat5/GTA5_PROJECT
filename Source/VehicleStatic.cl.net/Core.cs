﻿using CitizenFX.Core;
using System;
using SyncNet;
using System.Threading.Tasks;

public class Core : BaseScript
{
    public static void Call(ulong hashCode, params InputArgument[] args) => Function.Call((Hash)hashCode, args);
    public static void Call<T>(ulong hashCode, params InputArgument[] args) => Function.Call<T>((Hash)hashCode, args);
    public Core()
    {
        Tick += Update;
        EventHandlers["onResourceStart"] += new Action(ResourceStart);
        EventHandlers["onResourceStop"] += new Action(ResourceStop);
        EventHandlers["playerSpawned"] += new Action(PlayerSpawned);

        Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onEnteringVehicle") onEnteringVehicle(new Vehicle(data[0]), (VehicleSeat)data[1]); });
        Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onEnteringAborted") onEnteringAborted(new Vehicle(data[0]), (VehicleSeat)data[1]); });
        Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onEnteredVehicle") onEnteredVehicle(new Vehicle(data[0]), (VehicleSeat)data[1]); });
        Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onLeftVehicleJump") onLeftVehicleJump(new Vehicle(data[0]), (VehicleSeat)data[1]); });
        Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onLeftVehicle") onLeftVehicle(new Vehicle(data[0]), (VehicleSeat)data[1]); });
        Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onPlayerKilled") onPlayerKilled(data); });
        Network.onLocalTrigger += new Network.LocalTriggerDelegate((trigger) => { if (trigger == "onPlayerDied") onPlayerDied(); });
        Network.onLocalTrigger += new Network.LocalTriggerDelegate((trigger) => { if (trigger == "onPlayerWasted") onPlayerWasted(); });
    }


    #region EVENTS
    public delegate void PlayerKilledDelegate(int killer);
    public delegate void VehicleDelegate(Vehicle vehicle, VehicleSeat seat);
    public delegate void EmptyDelegate();
    public static event VehicleDelegate onEnteringVehicle = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
    public static event VehicleDelegate onEnteringAborted = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
    public static event VehicleDelegate onEnteredVehicle = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
    public static event VehicleDelegate onLeftVehicle = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
    public static event VehicleDelegate onLeftVehicleJump = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
    public static event EmptyDelegate onPlayerDied = new EmptyDelegate(new EmptyDelegate(() => { }));
    public static event EmptyDelegate onPlayerWasted = new EmptyDelegate(new EmptyDelegate(() => { }));
    public static event PlayerKilledDelegate onPlayerKilled = new PlayerKilledDelegate(new PlayerKilledDelegate((killer) => { }));
    #endregion

    virtual public async Task Update() { await Delay(0); }
    virtual public void ResourceStart() { }
    virtual public void ResourceStop() { }
    virtual public void PlayerSpawned() { }
}


namespace SyncNet
{
    public class Network : BaseScript
    {
        public Network()
        {
            //Trigger
            EventHandlers["FX:CLIENT:TRIGGER"] += new Action<int, string>((sender, trigger) => onNetworkTrigger(Players[sender], trigger));
            EventHandlers["FX:CLIENT:SERVER:TRIGGER"] += new Action<string>(trigger => onServerTriggerPrivate(trigger));

            //Data
            EventHandlers["FX:CLIENT:DATA"] += new Action<int, string, dynamic>((sender, trigger, data) => onNetworkData(Players[sender], trigger, data));
            EventHandlers["FX:CLIENT:SERVER:DATA"] += new Action<string, dynamic>((trigger, data) => onServerData(trigger, data));

            //Local
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>(trigger => onLocalTrigger(trigger));
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>((trigger, data) => onLocalData(trigger, data));
        }

        //LOCAL TRIGGER
        public delegate void LocalTriggerDelegate(string triggerName);
        static public event LocalTriggerDelegate onLocalTrigger = new LocalTriggerDelegate(new LocalTriggerDelegate(trigger => { }));

        //LOCAL DATA
        public delegate void LocalDataDelegate(string triggerName, dynamic data);
        static public event LocalDataDelegate onLocalData = new LocalDataDelegate(new LocalDataDelegate((trigger, data) => { }));

        //TRIGGER
        public delegate void NetworkTriggerDelegate(Player sender, string triggerName);
        static public event NetworkTriggerDelegate onNetworkTrigger = new NetworkTriggerDelegate(new NetworkTriggerDelegate((sender, trigger) => { }));

        //TRIGGER SERVER
        public delegate void ServerTriggerPrivateDelegate(string triggerName);
        static public event ServerTriggerPrivateDelegate onServerTriggerPrivate = new ServerTriggerPrivateDelegate(new ServerTriggerPrivateDelegate((trigger) => { }));

        //DATA
        public delegate void NetworkDataDelegate(Player sender, string triggerName, dynamic data);
        static public event NetworkDataDelegate onNetworkData = new NetworkDataDelegate(new NetworkDataDelegate((sender, trigger, data) => { }));

        //DATA SERVER
        public delegate void ServerDataDelegate(string triggerName, dynamic data);
        static public event ServerDataDelegate onServerData = new ServerDataDelegate(new ServerDataDelegate((trigger, data) => { }));

        //== [ CMD ] ==

        //LOCAL CMD
        static public void Local(string triggerName)
        {
            TriggerEvent("FX:LOCAL:TRIGGER", triggerName);
            Debug.WriteLine("[LOCAL][SEND] TRIGGER: {0}", triggerName);
        }
        static public void Local<T>(string triggerName, T data)
        {
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
            Debug.WriteLine("[LOCAL][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }

        static public void Local<T>(string triggerName, params T[] data)
        {
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
            Debug.WriteLine("[LOCAL][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }

        //SERVER CMD
        static public void Server(string triggerName)
        {
            TriggerServerEvent("FX:SERVER:CLIENT:TRIGGER", triggerName);
            Debug.WriteLine("[SERVER][SEND] TRIGGER: {0}", triggerName);
        }
        static public void Server<T>(string triggerName, T data)
        {
            TriggerServerEvent("FX:SERVER:CLIENT:DATA", triggerName, data);
            Debug.WriteLine("[SERVER][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }
        static public void Server<T>(string triggerName, params T[] data) => Local(triggerName, data);

        //TRIGGER
        static public void Send(Player player, string triggerName)
        {
            TriggerServerEvent("FX:SERVER:TRIGGER", triggerName, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FOR: {1}", triggerName, player.Name);
        }
        static public void Send(string triggerName) => Send(Game.Player, triggerName);

        //PACKET
        static public void Send(Player player, Action data)
        {
            TriggerServerEvent("FX:SERVER:PACKET", data, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] PACKET: {0}, FOR: {1}", data, player.Name);
        }
        static public void Send(Action data) => Send(Game.Player, data);

        //DATA
        static public void Send<T>(Player player, string triggerName, T data)
        {
            TriggerServerEvent("FX:SERVER:DATA", triggerName, data, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FOR: {3}", triggerName, data.GetType().Name, data, player.Name);
        }
        static public void Send<T>(string triggerName, T data) => Send(Game.Player, triggerName, data);
        static public void Send<T>(string triggerName, params T[] data) => Send(triggerName, data);
        static public void Send<T>(Player player, string triggerName, params T[] data) => Send(player, triggerName, data);
    }
}

namespace CoreExtensions
{
    public static class _entity
    {
        public static Vector3 Forward(this Entity ent) => GameMath.RotationToDirection(ent.Rotation);
        public static Vector3 Backward(this Entity ent) => -Forward(ent);
        public static Vector3 Right(this Entity ent) => Vector3.Cross(Forward(ent), Vector3.UnitZ);
        public static Vector3 Left(this Entity ent) => -Right(ent);
    }
    public static class _ped
    {
        static public void SetCanAttackFriendly(this Ped ped, bool enable) => Function.Call(Hash.SET_CAN_ATTACK_FRIENDLY, ped.Handle, enable);
        static public void SetPedCanBeDraggedOut(this Ped ped, bool enable) => Function.Call(Hash.SET_PED_CAN_BE_DRAGGED_OUT, ped.Handle, enable);
        static public void SetDriveCruiseSpeed(this Ped ped, float speed) => Function.Call(Hash.SET_DRIVE_TASK_CRUISE_SPEED, ped.Handle, speed);
        static public void SetDriveMaxCruiseSpeed(this Ped ped, float speed) => Function.Call(Hash.SET_DRIVE_TASK_MAX_CRUISE_SPEED, ped.Handle, speed);
        static public VehicleSeat IsSeatIndex(this Ped ped)
        {
            Vehicle veh = ped.CurrentVehicle;
            foreach (var i in Enum.GetValues(typeof(VehicleSeat)))
            {
                if (veh.IsSeatFree((VehicleSeat)i)) continue;
                if (veh.GetPedOnSeat((VehicleSeat)i) == Game.PlayerPed) return (VehicleSeat)i;
            }
            return VehicleSeat.None;
        }
    }
}

public enum Keys
{
    ESC = 322, F1 = 288, F2 = 289, F3 = 170, F5 = 166, F6 = 167, F7 = 168, F8 = 169, F9 = 56, F10 = 57,
    Tilde = 243, D1 = 157, D2 = 158, D3 = 160, D4 = 164, D5 = 165, D6 = 159, D7 = 161, D8 = 162, D9 = 163, Minus = 84, Plus = 83, BACKSPACE = 177,
    TAB = 37, Q = 44, W = 32, E = 38, R = 45, T = 245, Y = 246, U = 303, P = 199, OpenBrackets = 39, CloseBrackets = 40, ENTER = 18,//OpenBrackets = [, CloseBrackets = ]
    CAPS = 137, A = 34, S = 8, D = 9, F = 23, G = 47, H = 74, K = 311, L = 182,
    LEFTSHIFT = 21, Z = 20, X = 73, C = 26, V = 0, B = 29, N = 249, M = 244, PointLeft = 82, Point = 81,//PointLeft = , Point = .
    LEFTCTRL = 36, LEFTALT = 19, SPACE = 22, RIGHTCTRL = 70,
    HOME = 213, PAGEUP = 10, PAGEDOWN = 11, DELETE = 178,
    LEFT = 174, RIGHT = 175, TOP = 27, DOWN = 173,
    NENTER = 201, N4 = 108, N5 = 60, N6 = 107, NPlus = 96, NMinus = 97, N7 = 117, N8 = 61, N9 = 118
}