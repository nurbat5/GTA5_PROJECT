﻿using System;
using CitizenFX.Core;
using CitizenFX.Core.Native;

public enum DecorationType
{
    Float = 1,
    Bool = 2,
    Int = 3,
    Time = 5
}

public static class EntityDecoration
{
    internal static Type floatType = typeof(float);
    internal static Type boolType = typeof(bool);
    internal static Type intType = typeof(int);

    public static bool ExistOn(Entity entity, string propertyName) => Function.Call<bool>(Hash.DECOR_EXIST_ON, entity.NativeValue, propertyName);
    public static bool HasDecor(this Entity ent, string propertyName) => ExistOn(ent, propertyName);
    public static void RegisterProperty(string propertyName, DecorationType type) => Function.Call(Hash.DECOR_REGISTER, propertyName, (int)type);
    public static void SetDecor(this Entity ent, string propertyName, float value) => Function.Call(Hash._DECOR_SET_FLOAT, ent.NativeValue, propertyName, value);
    public static void SetDecor(this Entity ent, string propertyName, int value) => Function.Call(Hash.DECOR_SET_INT, ent.NativeValue, propertyName, value);
    public static void SetDecor(this Entity ent, string propertyName, bool value) => Function.Call(Hash.DECOR_SET_BOOL, ent.NativeValue, propertyName, value);
    public static T GetDecor<T>(this Entity ent, string propertyName)
    {
        if (!ExistOn(ent, propertyName)) throw new EntityDecorationUnregisteredPropertyException();

        Type genericType = typeof(T);
        Hash nativeMethod;

        if (genericType == floatType) nativeMethod = Hash._DECOR_GET_FLOAT;
        else if (genericType == intType) nativeMethod = Hash.DECOR_GET_INT;
        else if (genericType == boolType) nativeMethod = Hash.DECOR_GET_BOOL;
        else throw new EntityDecorationUndefinedTypeException();

        return (T)Function.Call<T>(nativeMethod, ent.NativeValue, propertyName);
    }
}

public class EntityDecorationUnregisteredPropertyException : Exception { }
public class EntityDecorationUndefinedTypeException : Exception { }
