﻿using System.Collections.Generic;
using CitizenFX.Core;

namespace Fuel
{
    public static class PetrolTanks
    {
        static public void SetFuel(Vehicle vehicle, float addFuelValue)
        {
            float max = FuelLevelMax(vehicle);
            float fuelLevel = FuelLevel(vehicle);
            if (fuelLevel < 0 && addFuelValue < 0) return;
            if (fuelLevel > max && addFuelValue > 0) return;
            vehicle.FuelLevel = (fuelLevel + addFuelValue) / max * 100;
            vehicle.SetDecor("_Fuel_Level", (fuelLevel + addFuelValue));
        }
        static public float FuelLevel(Vehicle vehicle) => vehicle.HasDecor("_Fuel_Level") ? vehicle.GetDecor<float>("_Fuel_Level") : FuelLevelMax(vehicle);
        static public int FuelLevelMax(Vehicle vehicle) => fuelLevels.ContainsKey((VehicleHash)vehicle.Model.Hash) ? fuelLevels[(VehicleHash)vehicle.Model.Hash] : 65;
        static private Dictionary<VehicleHash, int> fuelLevels = new Dictionary<VehicleHash, int>
        {
            { VehicleHash.Police, 100 }
        };
    }
}
