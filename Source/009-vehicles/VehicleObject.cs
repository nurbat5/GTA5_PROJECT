﻿using CitizenFX.Core;

namespace _009_vehicles
{
    public class VehicleObject
    {
        //Строго NATIVE !!
        public class VehicleModel
        {
            public int ID { get; set; } = -1;
            public int Handle { get; set; } = 0;
            public ulong Model { get; set; } = 3950024287; //Blista
            public float[] Position { get; set; } = new float[3];
            public float[] Rotation { get; set; } = new float[3];
            public float BodyHealth { get; set; } = 1000;
            public bool CanTiresBurst { get; set; } = false;
            public float DirtLevel { get; set; } = 0;
            public float EngineHealth { get; set; } = 1000;
            public float FuelLevel { get; set; } = 100;
            public bool IsAlarm { get; set; } = false;
            public bool IsBurnout { get; set; } = false;
            public bool IsDriveable { get; set; } = true;
            public bool IsEngineRunning { get; set; } = false;
            public bool IsHandbrake { get; set; } = false;
            public bool IsSirenActive { get; set; } = false;
            public bool IsStolen { get; set; } = false;
            public bool IsTaxiLightOn { get; set; } = false;
            public bool IsWanted { get; set; } = false;
            public int LockStatus { get; set; } = (int)VehicleLockStatus.Unlocked;
            public bool NeedsToBeHotwired { get; set; } = false;
            public float OilLevel { get; set; } = 100;
            public float PetrolTankHealth { get; set; } = 1000;
        }

        protected VehicleModel Vehicle = new VehicleModel();
        public VehicleObject(dynamic data)
        {
            Vehicle.ID = data.ID;
            Vehicle.Handle = data.Handle;
            Vehicle.Model = data.Model;
            Vehicle.Position = data.Position;
            Vehicle.Rotation = data.Rotation;
        }
        public Vector3 Position
        {
            get => new Vector3(Vehicle.Position[0], Vehicle.Position[1], Vehicle.Position[2]);
            set => Vehicle.Position = new float[3] { value.X, value.Y, value.Z };
        }
        public Vector3 Rotation
        {
            get => new Vector3(Vehicle.Rotation[0], Vehicle.Rotation[1], Vehicle.Rotation[2]);
            set => Vehicle.Rotation = new float[3] { value.X, value.Y, value.Z };
        }
        public VehicleHash Model
        {
            get => (VehicleHash)Vehicle.Model;
            set => Vehicle.Model = (ulong)value;
        }

        //Convert
        public bool Exist()
        {
            if (Vehicle.ID == -1 || Vehicle.Handle == 0) return false;
            var obj = Entity.FromHandle(Vehicle.Handle);
            if(obj != null && obj.Exists())
            {
                if (obj.Model.IsVehicle && obj.Model.Hash == (int)Vehicle.Model) return true;
            }
            return false;
        }
        public VehicleModel Object => Vehicle;
    }
}
