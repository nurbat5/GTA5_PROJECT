﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Security;
using System.Threading.Tasks;
using static LibSharp.CSharpCore.Core;

namespace LibSharp.CSharpCore
{
    public enum RadioNames
    {
        RADIO_01_CLASS_ROCK = 0, // Los Santos Rock Radio
        RADIO_02_POP = 1, // Non-Stop-Pop FM
        RADIO_03_HIPHOP_NEW = 2, // Radio Los Santos
        RADIO_04_PUNK = 3, // Channel X
        RADIO_05_TALK_01 = 4, // West Coast Talk Radio
        RADIO_06_COUNTRY = 5, // Rebel Radio
        RADIO_07_DANCE_01 = 6, // Soulwax FM
        RADIO_08_MEXICAN = 7, // East Los FM
        RADIO_09_HIPHOP_OLD = 8, // West Coast Classics
        RADIO_12_REGGAE = 9, // Blue Ark
        RADIO_13_JAZZ = 10, // Worldwide FM
        RADIO_14_DANCE_02 = 11, // FlyLo FM
        RADIO_15_MOTOWN = 12, // The Lowdown 91.1
        RADIO_20_THELAB = 13,// The Lab
        RADIO_16_SILVERLAKE = 14, // Radio Mirror Park
        RADIO_17_FUNK = 15,// Space 103.2
        RADIO_18_90S_ROCK = 16, // Vinewood Boulevard Radio
        RADIO_19_USER = 17, // Self Radio
        RADIO_11_TALK_02 = 18, // Blaine County Radio
        HIDDEN_RADIO_AMBIENT_TV_BRIGHT = 19,
        HIDDEN_RADIO_AMBIENT_TV = 20,
        HIDDEN_RADIO_ADVERTS = 21,
        HIDDEN_RADIO_01_CLASS_ROCK = 22,
        HIDDEN_RADIO_02_POP = 23,
        HIDDEN_RADIO_03_HIPHOP_NEW = 24,
        HIDDEN_RADIO_04_PUNK = 25,
        HIDDEN_RADIO_06_COUNTRY = 26,
        HIDDEN_RADIO_07_DANCE_01 = 27,
        HIDDEN_RADIO_09_HIPHOP_OLD = 28,
        HIDDEN_RADIO_12_REGGAE = 29,
        HIDDEN_RADIO_15_MOTOWN = 30,
        HIDDEN_RADIO_16_SILVERLAKE = 31,
        HIDDEN_RADIO_STRIP_CLUB = 32,
        HIDDEN_RADIO_BIKER_CLASSIC_ROCK = 33,
        HIDDEN_RADIO_BIKER_MODERN_ROCK = 34,
        HIDDEN_RADIO_BIKER_PUNK = 35,
        HIDDEN_RADIO_BIKER_HIP_HOP = 36,
        OFF = 255
    }

    public class CoreVehicle : Entity
    {
        #region Fields
        /*VehicleDoorCollection _doors;
        VehicleModCollection _mods;
        VehicleWheelCollection _wheels;
        VehicleWindowCollection _windows;*/

        public CoreVehicle(int handle) : base(handle) { }
        #endregion

        public string DisplayName => GetModelDisplayName(Model);
        public string LocalizedName => Game.GetGXTEntry(Call<string>(Hash.GET_DISPLAY_NAME_FROM_VEHICLE_MODEL, Model));
        public string ClassDisplayName => GetClassDisplayName(ClassType);
        public string ClassLocalizedName => Game.GetGXTEntry(ClassDisplayName);
        public VehicleClass ClassType => (VehicleClass)Call<int>(Hash.GET_VEHICLE_CLASS, Handle);
        public float BodyHealth
        {
            get => Call<float>(Hash.GET_VEHICLE_BODY_HEALTH, Handle);
            set => Call(Hash.SET_VEHICLE_BODY_HEALTH, Handle, value);
        }
        public float EngineHealth
        {
            get => Call<float>(Hash.GET_VEHICLE_ENGINE_HEALTH, Handle);
            set => Call(Hash.SET_VEHICLE_ENGINE_HEALTH, Handle, value);
        }
        public float PetrolTankHealth
        {
            get => Call<float>(Hash.GET_VEHICLE_PETROL_TANK_HEALTH, Handle);
            set => Call(Hash.SET_VEHICLE_PETROL_TANK_HEALTH, Handle, value);
        }
        public float FuelLevel
        {
            get => Call<float>(0x5f739bb8, Handle);
            set => Call(0xba970511, Handle, value);
        }
        public float OilLevel
        {
            get => Call<float>(0xfc7f8ef4, Handle);
            set => Call(0x90d1cad1, Handle, value);
        }
        public float Gravity
        {
            get => Call<float>(0xd0175e0b, Handle);
            set => Call(0x7b2a6dc, Handle, value);
        }
        public bool IsEngineRunning
        {
            get => Call<bool>(Hash.GET_IS_VEHICLE_ENGINE_RUNNING, Handle);
            set => Call(Hash.SET_VEHICLE_ENGINE_ON, Handle, value, true);
        }
        public bool IsEngineStarting => Call<bool>(0xbb340d04, Handle);
        public bool IsRadioEnabled
        {
            set => Call(Hash.SET_VEHICLE_RADIO_ENABLED, Handle, value);
        }
        public RadioStation RadioStation
        {
            set
            {
                if (value == RadioStation.RadioOff) Function.Call(Hash.SET_VEH_RADIO_STATION, "OFF");
                else if (Enum.IsDefined(typeof(RadioStation), value)) Function.Call(Hash.SET_VEH_RADIO_STATION, ((RadioNames)(int)value).ToString());
            }
        }
        public float Speed
        {
            get => Call<float>(Hash.GET_ENTITY_SPEED, Handle);
            set
            {
                if (Model.IsTrain)
                {
                    Call(Hash.SET_TRAIN_SPEED, Handle, value);
                    Call(Hash.SET_TRAIN_CRUISE_SPEED, Handle, value);
                }
                else
                {
                    Call(Hash.SET_VEHICLE_FORWARD_SPEED, Handle, value);
                }
            }
        }
        public float WheelSpeed => Core.Call<float>(0x149c9da0, Handle);
        public float Acceleration => Core.Call<float>(0x478321, Handle);
        public float CurrentRPM
        {
            get => Call<float>(0xe7b12b54, Handle);
            set => Call(0x2a01a8fc, Handle, value);
        }
        public float HighGear
        {
            get => Call<float>(0xf1d1d689, Handle);
            set => Call(0x20b1b3e6, Handle, value);
        }
        public int CurrentGear => Call<int>(0xb4f4e566, Handle);
        public float SteeringAngle => Call<float>(0x1382fcea, Handle);
        public float SteeringScale
        {
            get => Call<float>(0x954465de, Handle);
            set => Call(0xeb46596f, Handle, value);
        }
        public bool HasForks => Bones.HasBone("forks");

        public bool IsAlarmSet
        {
            set => Function.Call(Hash.SET_VEHICLE_ALARM, Handle, value);
            get => Call<bool>(Hash.IS_VEHICLE_ALARM_ACTIVATED, Handle);
        }
        public bool IsAlarmSounding => Function.Call<bool>(Hash.IS_VEHICLE_ALARM_ACTIVATED, Handle);
        public int AlarmTimeLeft
        {
            get => Call<int>(0xc62aac98, Handle);
            set => Call(0xc108ee6f, Handle, value);
        }
        public void StartAlarm() => Function.Call(Hash.START_VEHICLE_ALARM, Handle);

        public bool HasSiren => Bones.HasBone("siren1");
        public bool IsSirenActive
        {
            get => Function.Call<bool>(Hash.IS_VEHICLE_SIREN_ON, Handle);
            set => Function.Call(Hash.SET_VEHICLE_SIREN, Handle, value);
        }
        public bool IsSirenSilent
        {
            set => Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, Handle, value);
        }
        public void SoundHorn(int duration) => Function.Call(Hash.START_VEHICLE_HORN, Handle, duration, Game.GenerateHash("HELDDOWN"), 0);
        public bool IsWanted
        {
            get => Call<bool>(0xa7daf7c, Handle);
            set => Function.Call(Hash.SET_VEHICLE_IS_WANTED, Handle, value);
        }

        public bool ProvidesCover
        {
            set => Function.Call(Hash.SET_VEHICLE_PROVIDES_COVER, Handle, value);
        }

        public bool DropsMoneyOnExplosion
        {
            set => Function.Call(Hash._SET_VEHICLE_CREATES_MONEY_PICKUPS_WHEN_EXPLODED, Handle, value);
        }

        public bool Previously
        {
            get => Call<bool>(0xf849ed67, Handle);
            set => Function.Call(Hash.SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER, Handle, value);
        }

        public bool NeedsToBeHotwired
        {
            get => Call<bool>(0xf9933bf4, Handle);
            set => Function.Call(Hash.SET_VEHICLE_NEEDS_TO_BE_HOTWIRED, Handle, value);
        }

/*
        public bool AreLightsOn
        {
            [SecuritySafeCritical]
            get
            {
                bool lightState1, lightState2;
                unsafe
                {
                    Function.Call(Hash.GET_VEHICLE_LIGHTS_STATE, Handle, &lightState1, &lightState2);
                }
                return lightState1;
            }
            set => Function.Call(Hash.SET_VEHICLE_LIGHTS, Handle, value ? 3 : 4);
        }
        public bool AreHighBeamsOn
        {
            [SecuritySafeCritical]
            get
            {
                bool lightState1, lightState2;
                unsafe
                {
                    Function.Call(Hash.GET_VEHICLE_LIGHTS_STATE, Handle, &lightState1, &lightState2);
                }
                return lightState2;
            }
            set => Function.Call(Hash.SET_VEHICLE_FULLBEAM, Handle, value);
        }
        */

        public bool IsInteriorLightOn
        {
            get => Call<bool>(0xa411f72c, Handle);
            set => Function.Call(Hash.SET_VEHICLE_INTERIORLIGHT, Handle, value);
        }
        public bool IsSearchLightOn
        {
            get => Function.Call<bool>(Hash.IS_VEHICLE_SEARCHLIGHT_ON, Handle);
            set => Function.Call(Hash.SET_VEHICLE_SEARCHLIGHT, Handle, value, 0);
        }
        public bool IsTaxiLightOn
        {
            get => Function.Call<bool>(Hash.IS_TAXI_LIGHT_ON, Handle);
            set => Function.Call(Hash.SET_TAXI_LIGHTS, Handle, value);
        }
        public bool IsLeftIndicatorLightOn
        {
            set => Function.Call(Hash.SET_VEHICLE_INDICATOR_LIGHTS, Handle, true, value);
        }
        public bool IsRightIndicatorLightOn
        {
            set => Function.Call(Hash.SET_VEHICLE_INDICATOR_LIGHTS, Handle, false, value);
        }
        public bool IsHandbrakeForcedOn
        {
            set => Function.Call(Hash.SET_VEHICLE_HANDBRAKE, Handle, value);
        }
        public bool AreBrakeLightsOn
        {
            set => Function.Call(Hash.SET_VEHICLE_BRAKE_LIGHTS, Handle, value);
        }
        public float LightsMultiplier
        {
            set => Function.Call(Hash.SET_VEHICLE_LIGHT_MULTIPLIER, Handle, value);
        }

        public bool CanBeVisiblyDamaged
        {
            set => Function.Call(Hash.SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED, Handle, value);
        }

        public bool IsDamaged => Function.Call<bool>(Hash._IS_VEHICLE_DAMAGED, Handle);
        public bool IsDriveable
        {
            get => Function.Call<bool>(Hash.IS_VEHICLE_DRIVEABLE, Handle, 0);
            set => Function.Call(Hash.SET_VEHICLE_UNDRIVEABLE, Handle, !value);
        }
        public bool HasRoof
        {
            get => Function.Call<bool>(Hash.DOES_VEHICLE_HAVE_ROOF, Handle);
        }
        public bool IsLeftHeadLightBroken
        {
            get => Function.Call<bool>(Hash.GET_IS_LEFT_VEHICLE_HEADLIGHT_DAMAGED, Handle);
        }
        public bool IsRightHeadLightBroken
        {
            get => Function.Call<bool>(Hash.GET_IS_RIGHT_VEHICLE_HEADLIGHT_DAMAGED, Handle);
        }
        public bool IsRearBumperBrokenOff => Function.Call<bool>(Hash.IS_VEHICLE_BUMPER_BROKEN_OFF, Handle, false);
        public bool IsFrontBumperBrokenOff => Function.Call<bool>(Hash.IS_VEHICLE_BUMPER_BROKEN_OFF, Handle, true);

        public bool IsAxlesStrong
        {
            set => Function.Call<bool>(Hash.SET_VEHICLE_HAS_STRONG_AXLES, Handle, value);
        }

        public bool CanEngineDegrade
        {
            set => Function.Call(Hash.SET_VEHICLE_ENGINE_CAN_DEGRADE, Handle, value);
        }
        public float EnginePowerMultiplier
        {
            set => Function.Call(Hash._SET_VEHICLE_ENGINE_POWER_MULTIPLIER, Handle, value);
        }
        public float EngineTorqueMultiplier
        {
            set => Function.Call(Hash._SET_VEHICLE_ENGINE_TORQUE_MULTIPLIER, Handle, value);
        }

        public VehicleLandingGearState LandingGearState
        {
            get => Function.Call<VehicleLandingGearState>(Hash.GET_LANDING_GEAR_STATE, Handle);
            set => Function.Call(Hash._SET_VEHICLE_LANDING_GEAR, Handle, value);
        }
        public VehicleRoofState RoofState
        {
            get => Function.Call<VehicleRoofState>(Hash.GET_CONVERTIBLE_ROOF_STATE, Handle);
            set
            {
                switch (value)
                {
                    case VehicleRoofState.Closed:
                        Function.Call(Hash.RAISE_CONVERTIBLE_ROOF, Handle, true);
                        Function.Call(Hash.RAISE_CONVERTIBLE_ROOF, Handle, false);
                        break;
                    case VehicleRoofState.Closing:
                        Function.Call(Hash.RAISE_CONVERTIBLE_ROOF, Handle, false);
                        break;
                    case VehicleRoofState.Opened:
                        Function.Call(Hash.LOWER_CONVERTIBLE_ROOF, Handle, true);
                        Function.Call(Hash.LOWER_CONVERTIBLE_ROOF, Handle, false);
                        break;
                    case VehicleRoofState.Opening:
                        Function.Call(Hash.LOWER_CONVERTIBLE_ROOF, Handle, false);
                        break;
                }
            }
        }
        public VehicleLockStatus LockStatus
        {
            get => Function.Call<VehicleLockStatus>(Hash.GET_VEHICLE_DOOR_LOCK_STATUS, Handle);
            set => Function.Call(Hash.SET_VEHICLE_DOORS_LOCKED, Handle, value);
        }

        public float MaxBraking => Function.Call<float>(Hash.GET_VEHICLE_MAX_BRAKING, Handle);
        public float MaxTraction => Function.Call<float>(Hash.GET_VEHICLE_MAX_TRACTION, Handle);
        public bool IsOnAllWheels => Function.Call<bool>(Hash.IS_VEHICLE_ON_ALL_WHEELS, Handle);
        public bool IsStopped => Function.Call<bool>(Hash.IS_VEHICLE_STOPPED, Handle);
        public bool IsStoppedAtTrafficLights => Function.Call<bool>(Hash.IS_VEHICLE_STOPPED_AT_TRAFFIC_LIGHTS, Handle);

        public bool IsStolen
        {
            get => Function.Call<bool>(Hash.IS_VEHICLE_STOLEN, Handle);
            set => Function.Call(Hash.SET_VEHICLE_IS_STOLEN, Handle, value);
        }

        public bool IsConvertible => Function.Call<bool>(Hash.IS_VEHICLE_A_CONVERTIBLE, Handle, 0);

        public bool IsBurnoutForced
        {
            set => Function.Call<bool>(Hash.SET_VEHICLE_BURNOUT, Handle, value);
        }
        public bool IsInBurnout => Function.Call<bool>(Hash.IS_VEHICLE_IN_BURNOUT, Handle);

        public Ped Driver => GetPedOnSeat(VehicleSeat.Driver);
        public Ped[] Occupants
        {
            get
            {
                Ped driver = Driver;

                if (!Ped.Exists(driver))
                {
                    return Passengers;
                }

                var result = new Ped[PassengerCount + 1];
                result[0] = driver;

                for (int i = 0, j = 0, seats = PassengerCapacity; i < seats && j < result.Length; i++)
                {
                    if (!IsSeatFree((VehicleSeat)i))
                    {
                        result[j++ + 1] = GetPedOnSeat((VehicleSeat)i);
                    }
                }

                return result;
            }
        }
        public Ped[] Passengers
        {
            get
            {
                var result = new Ped[PassengerCount];

                if (result.Length == 0)
                {
                    return result;
                }

                for (int i = 0, j = 0, seats = PassengerCapacity; i < seats && j < result.Length; i++)
                {
                    if (!IsSeatFree((VehicleSeat)i))
                    {
                        result[j++] = GetPedOnSeat((VehicleSeat)i);
                    }
                }

                return result;
            }
        }
        public int PassengerCapacity => Function.Call<int>(Hash.GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS, Handle);
        public int PassengerCount => Function.Call<int>(Hash.GET_VEHICLE_NUMBER_OF_PASSENGERS, Handle);

        /*
                public VehicleDoorCollection Doors
                {
                    get
                    {
                        if (_doors == null)
                        {
                            _doors = new VehicleDoorCollection(this);
                        }

                        return _doors;
                    }
                }
                public VehicleModCollection Mods
                {
                    get
                    {
                        if (_mods == null)
                        {
                            _mods = new VehicleModCollection(this);
                        }

                        return _mods;
                    }
                }
                public VehicleWheelCollection Wheels
                {
                    get
                    {
                        if (_wheels == null)
                        {
                            _wheels = new VehicleWheelCollection(this);
                        }

                        return _wheels;
                    }
                }
                public VehicleWindowCollection Windows
                {
                    get
                    {
                        if (_windows == null)
                        {
                            _windows = new VehicleWindowCollection(this);
                        }

                        return _windows;
                    }
                }
                */


        public bool ExtraExists(int extra) => Function.Call<bool>(Hash.DOES_EXTRA_EXIST, Handle, extra);
        public bool IsExtraOn(int extra) => Function.Call<bool>(Hash.IS_VEHICLE_EXTRA_TURNED_ON, Handle, extra);
        public void ToggleExtra(int extra, bool toggle) => Function.Call(Hash.SET_VEHICLE_EXTRA, Handle, extra, !toggle);
        public Ped GetPedOnSeat(VehicleSeat seat) => new Ped(Function.Call<int>(Hash.GET_PED_IN_VEHICLE_SEAT, Handle, seat));
        public bool IsSeatFree(VehicleSeat seat) => Function.Call<bool>(Hash.IS_VEHICLE_SEAT_FREE, Handle, seat);

        public void Wash() => DirtLevel = 0f;
        public float DirtLevel
        {
            get => Function.Call<float>(Hash.GET_VEHICLE_DIRT_LEVEL, Handle);
            set => Function.Call(Hash.SET_VEHICLE_DIRT_LEVEL, Handle, value);
        }

        public bool PlaceOnGround() => Function.Call<bool>(Hash.SET_VEHICLE_ON_GROUND_PROPERLY, Handle);

        public void Repair() => Function.Call(Hash.SET_VEHICLE_FIXED, Handle);
        public void Explode() => Function.Call(Hash.EXPLODE_VEHICLE, Handle, true, false);

        public bool CanTiresBurst
        {
            get => Function.Call<bool>(Hash.GET_VEHICLE_TYRES_CAN_BURST, Handle);
            set => Function.Call(Hash.SET_VEHICLE_TYRES_CAN_BURST, Handle, value);
        }
        public bool CanWheelsBreak
        {
            set => Function.Call(Hash.SET_VEHICLE_WHEELS_CAN_BREAK, Handle, value);
        }

        public bool HasBombBay
        {
            get => Bones.HasBone("door_hatch_l") && Bones.HasBone("door_hatch_r");
        }
        public void OpenBombBay()
        {
            if (HasBombBay) Function.Call(Hash.OPEN_BOMB_BAY_DOORS, Handle);
        }
        public void CloseBombBay()
        {
            if (HasBombBay) Function.Call(Hash.CLOSE_BOMB_BAY_DOORS, Handle);
        }

        public void SetHeliYawPitchRollMult(float mult)
        {
            if (Model.IsHelicopter && mult >= 0 && mult <= 1) Function.Call(Hash._SET_HELICOPTER_ROLL_PITCH_YAW_MULT, Handle, mult);
        }

        public void DropCargobobHook(CargobobHook hook)
        {
            if (Model.IsCargobob) Function.Call(Hash._ENABLE_CARGOBOB_HOOK, Handle, hook);
        }
        public void RetractCargobobHook()
        {
            if (Model.IsCargobob) Function.Call(Hash._RETRACT_CARGOBOB_HOOK, Handle);
        }
        public bool IsCargobobHookActive()
        {
            if (Model.IsCargobob) return Function.Call<bool>(Hash._IS_CARGOBOB_HOOK_ACTIVE, Handle) || Function.Call<bool>(Hash._IS_CARGOBOB_MAGNET_ACTIVE, Handle);
            return false;
        }
        public bool IsCargobobHookActive(CargobobHook hook)
        {
            if (Model.IsCargobob)
            {
                switch (hook)
                {
                    case CargobobHook.Hook:
                        return Function.Call<bool>(Hash._IS_CARGOBOB_HOOK_ACTIVE, Handle);
                    case CargobobHook.Magnet:
                        return Function.Call<bool>(Hash._IS_CARGOBOB_MAGNET_ACTIVE, Handle);
                }
            }
            return false;
        }
        public void CargoBobMagnetGrabVehicle()
        {
            if (IsCargobobHookActive(CargobobHook.Magnet)) Function.Call(Hash._CARGOBOB_MAGNET_GRAB_VEHICLE, Handle, true);
        }
        public void CargoBobMagnetReleaseVehicle()
        {
            if (IsCargobobHookActive(CargobobHook.Magnet)) Function.Call(Hash._CARGOBOB_MAGNET_GRAB_VEHICLE, Handle, false);
        }

        public bool HasTowArm => Bones.HasBone("tow_arm");
        public float TowingCraneRaisedAmount
        {
            set => Function.Call(Hash._SET_TOW_TRUCK_CRANE_HEIGHT, Handle, value);
        }
        public Vehicle TowedVehicle => new Vehicle(Function.Call<int>(Hash.GET_ENTITY_ATTACHED_TO_TOW_TRUCK, Handle));
        public void TowVehicle(Vehicle vehicle, bool rear) => Function.Call(Hash.ATTACH_VEHICLE_TO_TOW_TRUCK, Handle, vehicle.Handle, rear, 0f, 0f, 0f);
        public void DetachFromTowTruck() => Function.Call(Hash.DETACH_VEHICLE_FROM_ANY_TOW_TRUCK, Handle);
        public void DetachTowedVehicle()
        {
            Vehicle vehicle = TowedVehicle;
            if (Exists(vehicle)) Function.Call(Hash.DETACH_VEHICLE_FROM_TOW_TRUCK, Handle, vehicle.Handle);
        }

        public void Deform(Vector3 position, float damageAmount, float radius) => Function.Call(Hash.SET_VEHICLE_DAMAGE, position.X, position.Y, position.Z, damageAmount, radius);

        public async Task<Ped> CreatePedOnSeat(VehicleSeat seat, Model model)
        {
            if (!IsSeatFree(seat))
            {
                throw new ArgumentException("The VehicleSeat selected was not free", "seat");
            }
            if (!model.IsPed || !await model.Request(1000))
            {
                return null;
            }

            return new Ped(Function.Call<int>(Hash.CREATE_PED_INSIDE_VEHICLE, Handle, 26, model.Hash, seat, 1, 1));
        }
        public Ped CreateRandomPedOnSeat(VehicleSeat seat)
        {
            if (!IsSeatFree(seat))
            {
                throw new ArgumentException("The VehicleSeat selected was not free", "seat");
            }
            if (seat == VehicleSeat.Driver)
            {
                return new Ped(Function.Call<int>(Hash.CREATE_RANDOM_PED_AS_DRIVER, Handle, true));
            }
            else
            {
                int pedHandle = Function.Call<int>(Hash.CREATE_RANDOM_PED, 0f, 0f, 0f);
                Function.Call(Hash.SET_PED_INTO_VEHICLE, pedHandle, Handle, seat);

                return new Ped(pedHandle);
            }
        }

        public static string GetModelDisplayName(Model vehicleModel) => Function.Call<string>(Hash.GET_DISPLAY_NAME_FROM_VEHICLE_MODEL, vehicleModel.Hash);

        public static VehicleClass GetModelClass(Model vehicleModel) => Function.Call<VehicleClass>(Hash.GET_VEHICLE_CLASS_FROM_NAME, vehicleModel.Hash);

        public static string GetClassDisplayName(VehicleClass vehicleClass) => "VEH_CLASS_" + ((int)vehicleClass).ToString();

        public new bool Exists() => Function.Call<int>(Hash.GET_ENTITY_TYPE, Handle) == 2;
        public static bool Exists(Vehicle vehicle) => !ReferenceEquals(vehicle, null) && vehicle.Exists();
    }
}
