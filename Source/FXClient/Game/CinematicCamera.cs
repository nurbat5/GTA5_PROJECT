﻿using CitizenFX.Core;
using FXClient.Core.Components;

namespace FXClient.Core
{
    public class CinematicCamera : Components.Camera
    {
        public CinematicCamera(int handle) : base(handle) { }
        public CinematicCamera(Vector3 position, Vector3 rotation, float fov) : base(position, rotation, fov) { }
        public float cameraSensitivity = 400;
        public float climbSpeed = 4;
        public float normalMoveSpeed = 10;
        public float slowMoveFactor = 0.25f;
        public float fastMoveFactor = 3;
        private float rotationX = 0.0f;
        private float rotationY = 0.0f;
        private float KeyRight
        {
            get
            {
                if (Game.IsControlPressed(0, Control.MoveRightOnly)) return 1;
                else if (Game.IsControlPressed(0, Control.MoveLeftOnly)) return -1;
                return 0;
            }
        }
        private float KeyForward
        {
            get
            {
                if (Game.IsControlPressed(0, Control.MoveUpOnly)) return 1;
                else if (Game.IsControlPressed(0, Control.MoveDownOnly)) return -1;
                return 0;
            }
        }
        public void Update()
        {
            rotationX += Game.GetDisabledControlNormal(0, (Control)220) * cameraSensitivity * Game.LastFrameTime;
            rotationY += Game.GetDisabledControlNormal(0, (Control)221) * cameraSensitivity * Game.LastFrameTime;
            Rotation = new Vector3(-rotationY, 0, -rotationX);

            Vector3 vec = Vector3.Zero;
            if (Game.IsControlPressed(0, Control.Sprint))
            {
                vec += Forward * (normalMoveSpeed * fastMoveFactor) * KeyForward * Game.LastFrameTime;
                vec += Right * (normalMoveSpeed * fastMoveFactor) * KeyRight * Game.LastFrameTime;//RIGHT
                if (Game.IsControlPressed(0, Control.Jump)) vec += Vector3.UnitZ * (climbSpeed * fastMoveFactor) * Game.LastFrameTime;
                if (Game.IsControlPressed(0, Control.MultiplayerInfo)) vec -= (Vector3.UnitZ * fastMoveFactor) * climbSpeed * Game.LastFrameTime;
            }
            else if (Game.IsControlPressed(0, Control.Duck))
            {
                vec += Forward * (normalMoveSpeed * slowMoveFactor) * KeyForward * Game.LastFrameTime;
                vec += Right * (normalMoveSpeed * slowMoveFactor) * KeyRight * Game.LastFrameTime;
                if (Game.IsControlPressed(0, Control.Jump)) vec += Vector3.UnitZ * (climbSpeed * slowMoveFactor) * Game.LastFrameTime;
                if (Game.IsControlPressed(0, Control.MultiplayerInfo)) vec -= (Vector3.UnitZ * slowMoveFactor) * climbSpeed * Game.LastFrameTime;
            }
            else
            {
                vec += Forward * normalMoveSpeed * KeyForward * Game.LastFrameTime;
                vec += Right * normalMoveSpeed * KeyRight * Game.LastFrameTime;//RIGHT
                if (Game.IsControlPressed(0, Control.Jump)) vec += Vector3.UnitZ * climbSpeed * Game.LastFrameTime;
                if (Game.IsControlPressed(0, Control.MultiplayerInfo)) vec -= Vector3.UnitZ * climbSpeed * Game.LastFrameTime;
            }
            Position += vec;
        }
    }
}
