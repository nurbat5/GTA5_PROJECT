﻿public enum eDispatchServiceType
{
    PoliceAutomobileDispatch = 1,
    PoliceHelicopterDispatch = 2,
    FireDepartmentDispatch = 3,
    SwatAutomobileDispatch = 4,
    AmbulanceDepartmentDispatch = 5,
    PoliceRidersDispatch = 6,
    PoliceVehicleRequest = 7,
    PoliceRoadBlockDispatch = 8,
    GangDispatch = 11,
    SwatHelicopterDispatch = 12,
    PoliceBoatDispatch = 13,
    ArmyVehicleDispatch = 14,
    BikerBackupDispatch = 15
};
