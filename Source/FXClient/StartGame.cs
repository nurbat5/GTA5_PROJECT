﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using FXClient.Core.Components;
using FXClient.Core.Config;
using FXClient.Core.Events;
using FXClient.Core.SyncNet;
using FXServer.Core.Components;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;


/*
 * Штаны 90-М 93-Ж
 * Start Camera POS: X:114.1408 Y:-1421.411 Z:38.20183 ROT: X:-23.21849 Y:9.290164E-07 Z:49.00742
 * */
namespace FXClient
{

    class StartGame : BaseScript
    {
        public StartGame()
        {
            Tick += INIT_TEST_Tick;
            PlayerEvents.OnSpawnConnected += PlayerConnected;
            //PlayerEvents.onCommand += NetworkWork_onCommand;
        }


        //SET_VEHICLE_CEILING_HEIGHT
        //GET_VEHICLE_DEFORMATION_GET_TREE 
        /*private async void NetworkWork_onCommand(string command, dynamic args)
        {
            if (command == "pos") Debug.WriteLine("POS: {0} ROT: {1} ", Game.PlayerPed.Position, Game.PlayerPed.Rotation);
            if (command == "delete") new Core.Components.Ped().CurrentVehicle.Delete();
            if (command == "create") Core.Components.Vehicle.Create(new Model(args[0]), Game.PlayerPed.Position).GetAwaiter();
            if (command == "per") new Core.Components.Vehicle().IsPersistent = true;
            if (command == "skin") new Core.Components.Player().ChangeModel(PedHash.Security01SMM).GetAwaiter();
            if (command == "respawn") { Game.Player.Character.Resurrect(); Game.Player.Character.IsCollisionEnabled = true; }
            if (command == "kill") { LocalPlayer.Character.Health = -1; }
            if (command == "tp") { LocalPlayer.Character.Position = Players[(string)args[0]].Character.Position; }
            if (command == "engine") { new Core.Components.Ped().CurrentVehicle.IsEngineRunning = !new Core.Components.Ped().CurrentVehicle.IsEngineRunning; }
            if (command == "create_ped")
            {
                var ped = await Core.Components.Ped.Create(new Model(PedHash.ShopKeep01), Core.Components.Ped.Shared.Position - Vector3.UnitZ, Core.Components.Ped.Shared.Heading);
                ped.PositionFrozen(true);
                ped.GodMod(true);
                ped.SetConfigFlag(46, true);
                ped.SetConfigFlag(0, false);
            }
        }*/
        private Task INIT_TEST_Tick()
        {
            if (Game.IsControlJustReleased(0, Control.MultiplayerInfo))
            {
                //Network.Send(new Action(async () => { await VehicleUnit.Create(new Model(VehicleHash.Police3), Game.PlayerPed.Position); }), PlayerUnit.Shared);
                //Network.Send("playerSpawned", PlayerUnit.Shared);
                //if (Game.PlayerPed.CurrentVehicle != null) Debug.WriteLine("POS: {0}, ROT: {1}, MODEL: {2}", Game.PlayerPed.CurrentVehicle.Position, Game.PlayerPed.CurrentVehicle.Rotation, Game.PlayerPed.CurrentVehicle.Model.Hash);
                var newCar = new VehicleStatic((uint)Game.PlayerPed.CurrentVehicle.Model.Hash, Game.PlayerPed.CurrentVehicle.Position, Game.PlayerPed.CurrentVehicle.Rotation);
                newCar.Color1 = Color.FromArgb(0, 0, 0);
                newCar.Color2 = Color.FromArgb(0, 0, 0);
                newCar.Save();
                //Network.Send("playerSpawned", PlayerUnit.Shared);
            }

            if (Game.IsControlJustPressed(0, Control.SelectCharacterMichael)) LocalPlayer.Character.Position = new Vector3(82, -1390, 29);
            return Task.FromResult(true);
        }

        private void PlayerConnected()
        {
            Function.Call(Hash.SHUTDOWN_LOADING_SCREEN);
            Game.Player.ChangeModel(PedHash.Natalia).GetAwaiter();
            Game.PlayerPed.Position = new Vector3(82, -1390, 29);
            //Network.Send("playerSpawned", PlayerUnit.Shared);
        }
    }
}
