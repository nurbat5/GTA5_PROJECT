﻿using CitizenFX.Core;
namespace FXClient.Core.Extension
{
    public static class _Entity
    {
        static public Vector3 Forward(this Entity ent) => GameMath.RotationToDirection(ent.Rotation);
        static public Vector3 Backward(this Entity ent) => -ent.Forward();
        static public Vector3 Right(this Entity ent) => Vector3.Cross(ent.Forward(), Vector3.UnitZ);
        static public Vector3 Left(this Entity ent) => -ent.Right();
    }
}
