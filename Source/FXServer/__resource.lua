server_script "FXServer.net.dll"
server_script "EntityFramework.dll"
server_script "MySql.Data.dll"
server_script "MySql.Data.Entity.EF6.dll"
server_script "EntityFramework.SqlServer.dll"
server_script "Newtonsoft.Json.net.dll"

client_script "FXClient.net.dll"
client_script "NativeUI.net.dll"

data_file 'HANDLING_FILE' 'handling.meta'
files {
    'handling.meta',
}
