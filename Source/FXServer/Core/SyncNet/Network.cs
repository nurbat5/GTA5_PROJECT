﻿using CitizenFX.Core;
using FXServer.Core.Components;
using System;

namespace FXServer.Core.SyncNet
{
    public class Network : BaseScript
    {
        #region NETWORK
        public Network()
        {
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>(FX_LOCAL_TRIGGER);
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>(FX_LOCAL_DATA);

            EventHandlers["FX:SERVER:TRIGGER"] += new Action<Player, string>(FX_SERVER_TRIGGER);
            EventHandlers["FX:SERVER:TRIGGER:PRIVATE"] += new Action<Player, string, int>(FX_SERVER_TRIGGER_PRIVATE);
            EventHandlers["FX:SERVER:DATA"] += new Action<Player, string, dynamic>(FX_SERVER_DATA);
            EventHandlers["FX:SERVER:DATA:PRIVATE"] += new Action<Player, string, dynamic, int>(FX_SERVER_DATA_PRIVATE);
        }

        //TRIGGER LOCAL
        private void FX_LOCAL_TRIGGER(string triggerName) => onLocalTrigger(triggerName);
        public delegate void onLocalTriggerDelegate(string triggerName);
        public static event onLocalTriggerDelegate onLocalTrigger = new onLocalTriggerDelegate((a0) => { });

        //DATA LOCAL
        private void FX_LOCAL_DATA(string triggerName, dynamic data) => onLocalData(triggerName, data);
        public delegate void onLocalDataDelegate(string triggerName, dynamic data);
        public static event onLocalDataDelegate onLocalData = new onLocalDataDelegate((a0, a1) => { });

        //TRIGGER
        private void FX_SERVER_TRIGGER([FromSource] Player fromPlayer, string triggerName) => onNetworkTrigger(new Client(fromPlayer), triggerName);
        public delegate void onNetworkTriggerDelegate(Client client, string triggerName);
        public static event onNetworkTriggerDelegate onNetworkTrigger = new onNetworkTriggerDelegate((a0, a1) => { });

        //TRIGGER PRIVATE
        private void FX_SERVER_TRIGGER_PRIVATE([FromSource] Player fromPlayer, string triggerName, int toPlayer) => onNetworkTriggerPrivate(new Client(fromPlayer), new Client(toPlayer), triggerName);
        public delegate void onNetworkTriggerPrivateDelegate(Client client, Client toClient, string triggerName);
        public static event onNetworkTriggerPrivateDelegate onNetworkTriggerPrivate = new onNetworkTriggerPrivateDelegate((a0, a1, a2) => { });

        //DATA
        private void FX_SERVER_DATA([FromSource] Player fromPlayer, string triggerName, dynamic data) => onNetworkData(new Client(fromPlayer), triggerName, data);
        public delegate void onNetworkDataDelegate(Client client, string triggerName, dynamic data);
        public static event onNetworkDataDelegate onNetworkData = new onNetworkDataDelegate((a0, a1, a2) => { });

        //DATA PRIVATE
        private void FX_SERVER_DATA_PRIVATE([FromSource] Player fromPlayer, string triggerName, dynamic data, int toPlayer) => onNetworkDataPrivate(new Client(fromPlayer), new Client(toPlayer), triggerName, data);
        public delegate void onNetworkDataPrivateDelegate(Client client, Client toClient, string triggerName, dynamic data);
        public static event onNetworkDataPrivateDelegate onNetworkDataPrivate = new onNetworkDataPrivateDelegate((a0, a1, a2, a3) => { });

        #endregion NETWORK

        //SERVER-SIDE => CLIENT-API

        //Trigger
        static public void Local(string triggerName)
        {
            Debug.WriteLine("[NETWORK][LOCAL] TRIGGER: {0}", triggerName);
            TriggerEvent("FX:LOCAL:TRIGGER", triggerName);
        }
        static public void Send(string triggerName)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}", triggerName);
            TriggerClientEvent("FX:CLIENT:TRIGGER", triggerName);
        }
        static public void Send(Client client, string triggerName)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FOR: {1}", triggerName, client.Name);
            TriggerClientEvent(client.Player, "FX:CLIENT:TRIGGER:PRIVATE", triggerName);
        }

        //Data
        static public void Local<T>(string triggerName, T data)
        {
            Debug.WriteLine("[NETWORK][LOCAL] EVENT: {0}; TYPE: {1}; DATA: {2};", triggerName, data.GetType(), data);
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
        }
        static public void Send<T>(string triggerName, T data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}; TYPE: {1}; DATA: {2};", triggerName, data.GetType(), data);
            TriggerClientEvent("FX:CLIENT:DATA", triggerName, data);
        }
        static public void Send<T>(string triggerName, T data, Client client)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}; TYPE: {1}; DATA: {2}; FOR: {3}", triggerName, data.GetType(), data, client.Name);
            TriggerClientEvent(client.Player, "FX:CLIENT:DATA:PRIVATE", triggerName, data);
        }
    }
}
