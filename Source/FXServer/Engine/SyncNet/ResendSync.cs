﻿using CitizenFX.Core;
using FXServer.Core.Components;
using FXServer.Core.SyncNet;

namespace FXServer.Engine.SyncNet
{
    public class ResendSync : BaseScript
    {
        public ResendSync()
        {
            Network.onNetworkTrigger += TRIGGER_SYNC;
            Network.onNetworkTriggerPrivate += TRIGGER_SYNC_PRIVATE;
            Network.onNetworkData += DATA_SYNC;
            Network.onNetworkDataPrivate += DATA_SYNC_PRIVATE;
        }

        private static void TRIGGER_SYNC(Client client, string triggerName)
        {
            Debug.WriteLine("[NETWORK][GET] FROM: {0}; TRIGGER: {1}", client.Name, triggerName);
            Network.Send(triggerName);
        }
        private static void TRIGGER_SYNC_PRIVATE(Client client, Client toClient, string triggerName)
        {
            Debug.WriteLine("[NETWORK][GET] FROM: {0}; TRIGGER: {1}; FOR: {2}", client.Name, triggerName, toClient.Name);
            if (client.Handle != toClient.Handle) Network.Send(toClient, triggerName);
        }
        private static void DATA_SYNC(Client client, string triggerName, dynamic data)
        {
            Debug.WriteLine("[NETWORK][GET] FROM: {0}; EVENT: {1}; DATA: {2}", client.Name, triggerName, data);
            Network.Send(triggerName, data);
        }
        private static void DATA_SYNC_PRIVATE(Client client, Client toClient, string triggerName, dynamic data)
        {
            Debug.WriteLine("[NETWORK][GET] FROM: {0}; EVENT: {1}; DATA: {2}; FOR: {3}", client.Name, triggerName, data, toClient.Name);
            if (client.Handle != toClient.Handle) Network.Send(triggerName, data, toClient);
        }

    }
}
