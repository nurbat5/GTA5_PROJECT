﻿using CitizenFX.Core;

namespace FXServer.Game.Data.Identity.Models
{
    public struct ePlayerSpawnPoint
    {
        public Vector3 Position;
        public Vector3 Rotation;
    }
}
