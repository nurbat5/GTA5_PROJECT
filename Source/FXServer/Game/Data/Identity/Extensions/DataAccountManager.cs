﻿using FXServer.Core.Data;
using FXServer.Core.Data.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace FXServer.Game.Data.Identity.Extensions
{
    public class DataAccountManager
    {
        private Account Account { get; set; }
        public DataAccountManager(Account Account) => this.Account = Account;
        public string this[string key]
        {
            set
            {
                using (CoreSQL SQL = new CoreSQL())
                {
                    //Exist
                    var _data = SQL.DataAccount.First(u => u.Key.Equals(key, StringComparison.CurrentCultureIgnoreCase) && u.UserId.Equals(Account.Id, StringComparison.CurrentCultureIgnoreCase));
                    if(_data != null)
                    {
                        _data.Value = value;
                        if (value == null || string.IsNullOrWhiteSpace(value)) SQL.Entry(_data).State = EntityState.Deleted; //Deleted is value NULL!!
                        else SQL.Entry(_data).State = EntityState.Modified; //Update Info
                        SQL.SaveChanges();
                        return;
                    }
                    //New Add
                    SQL.Entry(new DataAccount { Id = Guid.NewGuid().ToString(), Key = key, Value = value, UserId = Account.Id }).State = EntityState.Added;
                    SQL.SaveChanges();
                }
            }
            get
            {
                try
                {
                    using (CoreSQL SQL = new CoreSQL())
                    {
                        var _data = SQL.DataAccount.First(u => u.Key.Equals(key, StringComparison.CurrentCultureIgnoreCase) && u.UserId.Equals(Account.Id, StringComparison.CurrentCultureIgnoreCase));
                        return _data == null ? null : _data.Value;
                    }
                } catch { return null; }
            }
        }
    }
}
