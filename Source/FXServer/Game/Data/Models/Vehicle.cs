
using CitizenFX.Core;

namespace FXServer.Core.Data.Models
{
    public class Vehicle
    {
        public string Id { get; set; }
        public int Model { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public float Health { get; set; }
        public string Components { get; set; } = "{ }";
    }
}
