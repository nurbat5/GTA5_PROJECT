﻿namespace FXServer.Core.Data.Models
{
    public class DataAccount
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string UserId { get; set; }
    }
}
