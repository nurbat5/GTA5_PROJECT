﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using System;
using System.Threading.Tasks;

namespace BaseEvents
{
    public class VehicleEvents : Core
    {
        bool isInVehicle = false;
        bool isEnteringVehicle = false;
        Vehicle currentVehicle = null;
        int currentSeat = -3;
        public override async Task Update()
        {
            try
            {
                Ped ped = Game.PlayerPed;
                if (!isInVehicle && ped.IsAlive)
                {
                    Vehicle vehicleObject = ped.VehicleTryingToEnter;
                    if (vehicleObject != null && !isEnteringVehicle)
                    {
                        currentSeat = Function.Call<int>(Hash.GET_SEAT_PED_IS_TRYING_TO_ENTER, ped.Handle);
                        currentVehicle = vehicleObject;
                        Network.Local("onEnteringVehicle", (currentVehicle = vehicleObject).Handle, currentSeat);
                        isEnteringVehicle = true;
                    }
                    else if (vehicleObject == null && !ped.IsInVehicle() && isEnteringVehicle)
                    {
                        Network.Local("onEnteringAborted", currentVehicle.Handle, currentSeat);
                        isEnteringVehicle = false;
                        currentVehicle = null;
                        currentSeat = -3;
                    }
                    else if (ped.IsInVehicle())
                    {
                        isEnteringVehicle = false;
                        isInVehicle = true;
                        currentVehicle = ped.CurrentVehicle;
                        currentSeat = (int)ped.SeatIndex;
                        Network.Local("onEnteredVehicle", currentVehicle.Handle, currentSeat);
                    }
                }
                else if (isInVehicle)
                {
                    if (!ped.IsInVehicle() || ped.IsDead)
                    {
                        Network.Local("onLeftVehicle", currentVehicle.Handle, currentSeat);
                        isInVehicle = false;
                        currentSeat = -3;
                        currentVehicle = null;
                    }
                }
            }
            catch(Exception e) { Debug.WriteLine("VehicleEvents: {0}", e.Message); }
            await base.Update();
        }

    }
}
