﻿using CitizenFX.Core;
using LibSharp.CSharpCore;
using System.Threading.Tasks;

namespace BaseEvents
{
    public class PlayerEvents : Core
    {
        bool isDied = false;
        bool hasBeenDead = false;
        int diedAt = 0;
        public override Task Update()
        {
            Ped ped = Game.PlayerPed;
            if (ped.IsDead && !isDied)
            {
                isDied = true;
                if (diedAt == 0) diedAt = Game.GameTime;
                Entity killer = ped.GetKiller();
                if (killer != null && killer.Exists() && killer.Handle != 0)
                {
                    if (ped.Handle == killer.Handle)
                    {
                        Network.Local("onPlayerDied");
                        hasBeenDead = true;
                    }
                    else
                    {
                        Network.Local("onPlayerKilled", killer.Handle);
                        hasBeenDead = true;
                    }
                }
            }
            else if (ped.IsAlive && hasBeenDead)
            {
                isDied = false;
                diedAt = 0;
            }
            if (!hasBeenDead && diedAt > 0)
            {
                Network.Local("onPlayerWasted");
                hasBeenDead = true;
            }
            else if (hasBeenDead && diedAt <= 0) hasBeenDead = false;
            return base.Update();
        }
    }
}
