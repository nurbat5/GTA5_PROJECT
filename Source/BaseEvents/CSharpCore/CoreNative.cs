﻿using CitizenFX.Core.Native;

namespace LibSharp.CSharpCore
{
    public class CoreNative
    {
        public static void Call(Hash _hash, params InputArgument[] _args) => Function.Call(_hash, _args);
        public static T Call<T>(Hash _hash, params InputArgument[] _args) => Function.Call<T>(_hash, _args);
        public static void Call(ulong _hash, params InputArgument[] _args) => Function.Call((Hash)_hash, _args);
        public static T Call<T>(ulong _hash, params InputArgument[] _args) => Function.Call<T>((Hash)_hash, _args);
    }
}
