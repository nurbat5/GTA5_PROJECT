﻿using CitizenFX.Core;
using System;

namespace LibSharp.CSharpCore
{
    public class Network : BaseScript
    {

        public Network()
        {
            EventHandlers["FX:SERVER:CLIENT:TRIGGER"] += new Action<Player, string>(FX_CLIENT_TRIGGER);
            EventHandlers["FX:SERVER:CLIENT:DATA"] += new Action<Player, string, dynamic>(FX_CLIENT_DATA);

            EventHandlers["FX:SERVER:TRIGGER"] += new Action<Player, string, int>(FX_SERVER_TRIGGER);
            EventHandlers["FX:SERVER:DATA"] += new Action<Player, string, dynamic, int>(FX_SERVER_DATA);
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>(FX_LOCAL_TRIGGER);
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>(FX_LOCAL_DATA);
        }

        //TRIGGER LOCAL
        private void FX_LOCAL_TRIGGER(string triggerName) => onLocalTrigger(triggerName);
        public delegate void LocalTriggerDelegate(string triggerName);
        public static event LocalTriggerDelegate onLocalTrigger = new LocalTriggerDelegate(new LocalTriggerDelegate(trigger => { }));

        //DATA LOCAL
        private void FX_LOCAL_DATA(string triggerName, dynamic data) => onLocalData(triggerName, data);
        public delegate void LocalDataDelegate(string triggerName, dynamic data);
        public static event LocalDataDelegate onLocalData = new LocalDataDelegate(new LocalDataDelegate((trigger, data) => { }));

        //TRIGGER SERVER
        private void FX_CLIENT_TRIGGER([FromSource] Player fromPlayer, string triggerName) => onNetworkTriggerServer(fromPlayer, triggerName);
        public delegate void NetworkTriggerServerDelegate(Player fromPlayer, string triggerName);
        public static event NetworkTriggerServerDelegate onNetworkTriggerServer = new NetworkTriggerServerDelegate(new NetworkTriggerServerDelegate((player, data) => { }));

        //DATA SERVER
        private void FX_CLIENT_DATA([FromSource] Player fromPlayer, string triggerName, dynamic data) => onNetworkDataServer(fromPlayer, triggerName, data);
        public delegate void NetworkDataServerDelegate(Player fromPlayer, string triggerName, dynamic data);
        public static event NetworkDataServerDelegate onNetworkDataServer = new NetworkDataServerDelegate(new NetworkDataServerDelegate((player, trigger, data) => { }));

        //TRIGGER
        private void FX_SERVER_TRIGGER([FromSource] Player fromPlayer, string triggerName, int toPlayer) => onNetworkTrigger(fromPlayer, Players[toPlayer], triggerName);
        public delegate void NetworkTriggerDelegate(Player client, Player toPlayer, string triggerName);
        public static event NetworkTriggerDelegate onNetworkTrigger = new NetworkTriggerDelegate(new NetworkTriggerDelegate((player, toplayer, trigger) => { }));

        //DATA
        private void FX_SERVER_DATA([FromSource] Player fromPlayer, string triggerName, dynamic data, int toPlayer) => onNetworkData(fromPlayer, Players[toPlayer], triggerName, data);
        public delegate void NetworkDataDelegate(Player client, Player toPlayer, string triggerName, dynamic data);
        public static event NetworkDataDelegate onNetworkData = new NetworkDataDelegate(new NetworkDataDelegate((player, toplayer, trigger, data) => { }));

        //CMD
        static public void Local(string triggerName)
        {
            Debug.WriteLine("[LOCAL][SEND] TRIGGER: {0}", triggerName);
            TriggerEvent("FX:LOCAL:TRIGGER", triggerName);
        }
        static public void Local<T>(string triggerName, T data)
        {
            Debug.WriteLine("[LOCAL][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType(), data);
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
        }
        static public void Local<T>(string triggerName, params T[] data)
        {
            Debug.WriteLine("[LOCAL][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType(), data);
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
        }

        static public void Send(string triggerName)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FROM: SERVER", triggerName);
            TriggerClientEvent("FX:CLIENT:SERVER:TRIGGER", triggerName);
        }
        static public void Send(Player client, string triggerName)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FOR: {1}, FROM: SERVER", triggerName, client.Name);
            TriggerClientEvent(client, "FX:CLIENT:SERVER:TRIGGER", triggerName);
        }
        static public void Send<T>(string triggerName, T data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FROM: SERVER", triggerName, data.GetType(), data);
            TriggerClientEvent("FX:CLIENT:SERVER:DATA", triggerName, data);
        }
        static public void Send<T>(Player client, string triggerName, T data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FOR: {3}, FROM: SERVER", triggerName, data.GetType(), data, client.Name);
            TriggerClientEvent(client, "FX:CLIENT:SERVER:DATA", triggerName, data);
        }
        static public void Send<T>(string triggerName, params T[] data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FROM: SERVER", triggerName, data.GetType(), data);
            TriggerClientEvent("FX:CLIENT:SERVER:DATA", triggerName, data);
        }
        static public void Send<T>(Player client, string triggerName, params T[] data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FOR: {3}, FROM: SERVER", triggerName, data.GetType(), data, client.Name);
            TriggerClientEvent(client, "FX:CLIENT:SERVER:DATA", triggerName, data);
        }
    }
}
