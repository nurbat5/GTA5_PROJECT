﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using System.Collections.Generic;
using System.Linq;

namespace _009S_vehicles
{
    public class Init : Core
    {
        static int AI = 0;
        public static Dictionary<int, Vector3> CarPositions = new Dictionary<int, Vector3>();
        public static Dictionary<int, dynamic> CarData = new Dictionary<int, dynamic>();

        public override void ResourceStart()
        {
            Network.onNetworkDataServer += GET_PLAYER_POS;
            Network.onLocalData += GET_SERVER_VEHICLE;
        }

        //Локальные команды
        void UPDATE_VEHICLE(dynamic data)
        {
            CarData[data.ID] = data;
            CarPositions[data.ID] = new Vector3(data.Position[0], data.Position[1], data.Position[2]);
        }
        void ADD_VEHICLE(dynamic data)
        {
            data.ID = AI; //Write Server VehicleID
            CarData[AI] = data;
            CarPositions[AI] = new Vector3(data.Position[0], data.Position[1], data.Position[2]);
            AI++;
        }
        
        //Отправка Машин, подходящие под правило
        void SEND_PLAYER(Player player, Vector3 position, float range = 400)
        {
            var ID_VEHICLES = CarPositions.Where(u => u.Value.DistanceToSquared(position) <= range).Select(u => u.Key);
            foreach (var id in ID_VEHICLES) Network.Send(player, "staticVehicle:sendVehicle", CarData.First(u => u.Key == id).Value);
        }

        //Обработка, запроса игрока

        //STEP 1, Получаем запрос от игрока
        private void GET_PLAYER_POS(Player fromPlayer, string triggerName, dynamic data)
        {
            if (triggerName != "vehicleStatic:Request") return;
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            new CreateVehicle(new Vector3(446.1397f, -1025.083f, 28.32789f), new Vector3(0.892441f, -0.9595076f, -176.782f)).Save();
            SEND_PLAYER(fromPlayer, new Vector3(446.1397f, -1025.083f, 28.32789f));
            Debug.WriteLine("({0}f, {1}f, {2}f) ROT: ({3}f, {4}f, {5}f)", data[0], data[1], data[2], data[3], data[4], data[5]);
        }

        //Получаем команду от сервера, на создание машины
        private void GET_SERVER_VEHICLE(string triggerName, dynamic data) { if (triggerName == "vehicleStatic:serverSave") ADD_VEHICLE(data); }

    }
}
