local autoparachute, blackout, mobileradio, dfps, 
	  dspeedo, nonpcstraffic, freezeradio, freezecurrenttime, 
	  radioname, explodevehicles, noclipmode, vibrator, 
	  showname1, showname2, cinecam, infiniteRocketBoost
local dropweapon = true
local prevCoordx = 0.0
local prevCoordy = 0.0
local prevCoordz = 0.0
local vibcount = 0
local fps = 0
local prevframes = 0
local curframes = 0
local prevtime = 0
local curtime = 0
local kmh = true --Set This To "true" and "mph" To "false" To Set KMH as Default (Speedometer)
local mph = false --Set This To "true" and "kmh" To "false" To Set MPH as Default (Speedometer)

RegisterNUICallback("misc", function(data, cb) --Miscellaneous Options
	local playerPed = GetPlayerPed(-1)
	local action = data.action
	local newstate = data.newstate

	if action == "autopara" then --Always Parachute
		autoparachute = newstate
		if (autoparachute == true) then
			drawNotification("~g~Always Parachute Enabled!")
		else
			drawNotification("~r~Always Parachute Disabled!")
		end
	elseif action == "blackoutonoff" then --Blackout
		blackout = newstate
		if (blackout == true) then
			drawNotification("~g~Blackout Enabled!")
		else
			drawNotification("~r~Blackout Disabled!")
		end
	elseif action == "mblradio" then --Mobile Radio
		mobileradio = newstate
		if (mobileradio == true) then
			drawNotification("~g~Mobile Radio Enabled!")
		else
			drawNotification("~r~Mobile Radio Disabled!")
		end
	elseif action == "drawfps" then --Draw FPS
		dfps = newstate
		if (dfps == true) then
			drawNotification("~g~Draw FPS Enabled!")
		else
			drawNotification("~r~Draw FPS Disabled!")
		end
	elseif action == "drawspeedo" then --Draw Speedometer
		dspeedo = newstate
		if (dspeedo == true) then
			drawNotification("~g~Speedometer Enabled!")
		else
			drawNotification("~r~Speedometer Disabled!")
		end
	elseif action == "drawspeedokmh" then --Set Speedometer (KMH)
		mph = false
		kmh = true
	elseif action == "drawspeedomph" then --Set Speedometer (MPH)
		kmh = false
		mph = true
	elseif action == "npcstraffic" then --No NPCs & Traffic
		nonpcstraffic = newstate
		if (nonpcstraffic == true) then
			drawNotification("~r~NPCs & Traffic Disabled!")
		else
			drawNotification("~g~NPCs & Traffic Enabled!")
		end
	elseif action == "explodevehs" then
		explodevehicles = newstate
		if (explodevehicles == true) then
			drawNotification("~g~Explode Nearest Vehicles Enabled!")
		else
			drawNotification("~r~Explode Nearest Vehicles Disabled!")
		end
	elseif action == "noclip" then
		noclipmode = newstate
		if (noclipmode == true) then
			drawNotification("~g~No Clip Mode Enabled!")
		else
			drawNotification("~r~No Clip Mode Disabled!")
			Citizen.InvokeNative(0xD86D101FCFD00A4B, GetPlayerPed(-1), 2)
			Citizen.InvokeNative(0xD86D101FCFD00A4B, GetPlayerPed(-1), 3)
			SetEntityCollision(GetPlayerPed(-1), true, true)
			FreezeEntityPosition(GetPlayerPed(-1), false)
		end
	elseif action == "vib" then --Vibrator
		vibrator = newstate
		if (vibrator == true) then
			drawNotification("~g~Vibrator Enabled!")
		else
			drawNotification("~r~Vibrator Disabled!")
		end
	elseif action == "cinematic" then --Vibrator
		cinecam = newstate
		if (cinecam == true) then
			drawNotification("~g~Cinematic Cam/ Button Disabled!")
		else
			drawNotification("~r~Cinematic Cam/ Button Enabled!")
		end
	elseif action == "rocket" then --Infinite Rocket Boost
		infiniteRocketBoost = newstate
		if (infiniteRocketBoost == true) then
			drawNotification("~g~Infinite Rocket Boost Disabled!")
		else
			drawNotification("~r~Infinite Rocket Boost Enabled!")
		end
	end

	cb("ok")
end)

RegisterNUICallback("radio", function(data, cb) --Radio Options
	local playerPed = GetPlayerPed(-1)

	if data.action == "skip" then --Skip Song (Radio)
		SkipRadioForward()
	elseif data.action == "unfreeze" then --Unfreeze Radio Station
		freezeradio = false
	else
		freezeradio = true
		radioname = data.action
	end

	cb("ok")
end)

RegisterNUICallback("weather", function(data, cb) --Weather Options
	local weather = data.action
	local weathername

	SetWeatherTypePersist(weather)
	SetWeatherTypeNowPersist(weather)
	SetWeatherTypeNow(weather)
	SetOverrideWeather(weather)
	
	if weather == "EXTRASUNNY" then
		weathername = "Extra Sunny"
	elseif weather == "SNOWLIGHT" then
		weathername = "Light Snow"
	else
		weathername = string.lower(weather)
	end
	
	drawNotification("~g~Weather changed to " .. FirstToUpper(weathername) .. ".")
	cb("ok")
end)

RegisterNUICallback("time", function(data, cb) --Time Options
	local hour
	local newstate = data.newstate
	
	if data.action == "6" or data.action == "12" or data.action == "18" or data.action == "0" then
		hour = tonumber(data.action)
		NetworkOverrideClockTime(hour, 0, 0)
		drawNotification("~g~Time changed to " .. hour .. ":00!")
	elseif data.action == "add" then
		if GetClockHours() == 23 then
			hour = 0
		else
			hour = GetClockHours() + 1
		end
		NetworkOverrideClockTime(hour, 0, 0)
		drawNotification("~g~Time changed to " .. hour .. ":00!")
	elseif data.action == "reduce" then
		if GetClockHours() == 0 then
			hour = 23
		else
			hour = GetClockHours() - 1
		end
		NetworkOverrideClockTime(hour, 0, 0)
		drawNotification("~g~Time changed to " .. hour .. ":00!")
	elseif data.action == "freeze" then
		freezecurrenttime = newstate
		if (freezecurrenttime == true) then
			drawNotification("~g~Freeze Current Time Enabled!")
		else
			drawNotification("~r~Freeze Current Time Disabled!")
		end
	end
	cb("ok")
end)

RegisterNUICallback("bogyguard", function(data, cb) --Bodyguard Options
	local playerPed = GetPlayerPed(-1)
	local action = data.action
	local GroupHandle = GetPlayerGroup(PlayerId())
	
	SetGroupSeparationRange(GroupHandle, 999999.9)

	if action == "spawn" then
		if (GetPedAsGroupMember(GroupHandle, 0) == 0) or (GetPedAsGroupMember(GroupHandle, 1) == 0) or (GetPedAsGroupMember(GroupHandle, 2) == 0) or (GetPedAsGroupMember(GroupHandle, 3) == 0) or (GetPedAsGroupMember(GroupHandle, 4) == 0) or (GetPedAsGroupMember(GroupHandle, 5) == 0) or (GetPedAsGroupMember(GroupHandle, 6) == 0) then
			local Ped = ClonePed(playerPed, GetEntityHeading(playerPed), 1, 1)
			local Pedblip = AddBlipForEntity(Ped)
			
			SetBlipSprite(Pedblip, 143)
			SetBlipColour(Pedblip, 3)
			
			SetPedAsGroupLeader(playerPed, GroupHandle)
			SetPedAsGroupMember(Ped, GroupHandle)
			SetPedNeverLeavesGroup(Ped, true)
			SetGroupFormation(GroupHandle, 2)
			
			SetEntityInvincible(Ped, true)
			SetPedCanBeTargetted(Ped, false)
			GiveWeaponToPed(Ped, GetHashKey("WEAPON_MINIGUN"), 999999999, false, true)
			SetPedInfiniteAmmo(Ped, true)
			SetPedInfiniteAmmoClip(Ped, true)
			
			
			drawNotification("~g~Bodyguard Spawned")
		else
			drawNotification("~r~Maximum 7 Bodyguards")
		end
	elseif action == "deleteall" then
		for i = 0, 6 do
			local ped = GetPedAsGroupMember(GroupHandle, i)
			RemoveBlip(GetBlipFromEntity(ped))
			SetPedNeverLeavesGroup(ped, false)
			RemovePedFromGroup(ped)
			SetEntityAsMissionEntity(ped, 1, 1)
			DeleteEntity(ped)
		end		
		drawNotification("~r~All Bodyguards Deleted")
	end

	cb("ok")
end)

RegisterNUICallback("bogyguarddelete", function(data, cb) --Bodyguard Delete Options
	local bguard = tonumber(data.action)
	local GroupHandle = GetPlayerGroup(PlayerId())
	local ped = GetPedAsGroupMember(GroupHandle, bguard - 1)
	
	RemoveBlip(GetBlipFromEntity(ped))
	SetPedNeverLeavesGroup(ped, false)
	RemovePedFromGroup(ped)
	SetEntityAsMissionEntity(ped, 1, 1)
	DeleteEntity(ped)
	drawNotification("~g~Bodyguard " .. bguard .. " Deleted!")

	cb("ok")
end)

RegisterNUICallback("spectate", function(data, cb) --Player Scenarios
	local player = tonumber(data.action)
	
	if data.action == "stop" then
		NetworkSetInSpectatorMode(1, GetPlayerPed(-1))
		NetworkSetInSpectatorMode(0, GetPlayerPed(-1))
		FreezeEntityPosition(GetPlayerPed(-1),  false)
		SetEntityCoords(GetPlayerPed(-1), prevCoordx, prevCoordy, prevCoordz, false, false, false, true)
		prevCoordx = 0.0
		prevCoordy = 0.0
		prevCoordz = 0.0
		drawNotification("~g~Spectating stopped!")
	else
		if GetPlayerPed(player) ~= 0 then
			if GetPlayerPed(-1) ~= GetPlayerPed(player) then
				if (prevCoordx == 0.0 and prevCoordy == 0.0 and prevCoordz == 0.0) or (prevCoordx == 9999.0 and prevCoordy == 9999.0 and prevCoordz == 9999.0) then
					local PlayerPedPos = GetEntityCoords(GetPlayerPed(-1), true)
					prevCoordx = PlayerPedPos.x
					prevCoordy = PlayerPedPos.y
					prevCoordz = PlayerPedPos.z
				end
				ClearPlayerWantedLevel(PlayerId())
				SetEntityCoords(GetPlayerPed(-1), 9999.0, 9999.0, 9999.0, false, false, false, true)
				FreezeEntityPosition(GetPlayerPed(-1),  true)
				RequestCollisionAtCoord(GetEntityCoords(GetPlayerPed(player), true))
				NetworkSetInSpectatorMode(1, GetPlayerPed(player))
				drawNotification("~g~Spectating Player " .. player + 1 .. "!")
			else
				drawNotification("~r~Can't Spectate Yourself!")
			end
		else
			drawNotification("~r~Player " .. player + 1 .. " Doesn't Exist!")
		end
	end

	cb("ok")
end)

RegisterNUICallback("onlineplayernames", function(data, cb) --Online Player Names Options
	local action = data.action

	if action == "show1" then
		drawNotification("~g~Showing Player Names (1 - 14)!~n~~y~Disappears In 30 Seconds!")
		showname1 = true
		showname2 = false
	elseif action == "show2" then
		drawNotification("~g~Showing Player Names (15 - 24)!~n~~y~Disappears In 30 Seconds!")
		showname1 = false
		showname2 = true
	end	

	cb("ok")
end)

Citizen.CreateThread(function() --Show Player Names
	while true do
		Citizen.Wait(0)
		
		if (showname1 == true) then
			CustomDraw("~s~Player 1: ~n~~r~" .. GetPlayerName(0) .. "", 0.950, 0.150)
			CustomDraw("~s~Player 2: ~n~~r~" .. GetPlayerName(1) .. "", 0.950, 0.200)
			CustomDraw("~s~Player 3: ~n~~r~" .. GetPlayerName(2) .. "", 0.950, 0.250)
			CustomDraw("~s~Player 4: ~n~~r~" .. GetPlayerName(3) .. "", 0.950, 0.300)
			CustomDraw("~s~Player 5: ~n~~r~" .. GetPlayerName(4) .. "", 0.950, 0.350)
			CustomDraw("~s~Player 6: ~n~~r~" .. GetPlayerName(5) .. "", 0.950, 0.400)
			CustomDraw("~s~Player 7: ~n~~r~" .. GetPlayerName(6) .. "", 0.950, 0.450)
			CustomDraw("~s~Player 8: ~n~~r~" .. GetPlayerName(7) .. "", 0.950, 0.500)
			CustomDraw("~s~Player 9: ~n~~r~" .. GetPlayerName(8) .. "", 0.950, 0.550)
			CustomDraw("~s~Player 10: ~n~~r~" .. GetPlayerName(9) .. "", 0.950, 0.600)
			CustomDraw("~s~Player 11: ~n~~r~" .. GetPlayerName(10) .. "", 0.950, 0.650)
			CustomDraw("~s~Player 12: ~n~~r~" .. GetPlayerName(11) .. "", 0.950, 0.700)
			CustomDraw("~s~Player 13: ~n~~r~" .. GetPlayerName(12) .. "", 0.950, 0.750)
			CustomDraw("~s~Player 14: ~n~~r~" .. GetPlayerName(13) .. "", 0.950, 0.800)
		elseif (showname2 == true) then
			CustomDraw("~s~Player 15: ~n~~r~" .. GetPlayerName(14) .. "", 0.950, 0.150)
			CustomDraw("~s~Player 16: ~n~~r~" .. GetPlayerName(15) .. "", 0.950, 0.200)
			CustomDraw("~s~Player 17: ~n~~r~" .. GetPlayerName(16) .. "", 0.950, 0.250)
			CustomDraw("~s~Player 18: ~n~~r~" .. GetPlayerName(17) .. "", 0.950, 0.300)
			CustomDraw("~s~Player 19: ~n~~r~" .. GetPlayerName(18) .. "", 0.950, 0.350)
			CustomDraw("~s~Player 20: ~n~~r~" .. GetPlayerName(19) .. "", 0.950, 0.400)
			CustomDraw("~s~Player 21: ~n~~r~" .. GetPlayerName(20) .. "", 0.950, 0.450)
			CustomDraw("~s~Player 22: ~n~~r~" .. GetPlayerName(21) .. "", 0.950, 0.500)
			CustomDraw("~s~Player 23: ~n~~r~" .. GetPlayerName(22) .. "", 0.950, 0.550)
			CustomDraw("~s~Player 24: ~n~~r~" .. GetPlayerName(23) .. "", 0.950, 0.600)
		end
	end
end)

Citizen.CreateThread(function() --Hide Player Names After 30 Seconds
	while true do
		Citizen.Wait(0)
		
		if (showname1 == true) then
			Citizen.Wait(30000)
			showname1 = false
		elseif (showname2 == true) then
			Citizen.Wait(30000)
			showname2 = false
		end
	end
end)

Citizen.CreateThread(function() --Always Parachute
	while true do
		Citizen.Wait(0)

		if OnlyForAdmins then
			if admin then
				if (autoparachute == true) then
					GiveWeaponToPed(GetPlayerPed(-1), GetHashKey("gadget_parachute"), 1, false, false)
				end
			end
		else
			if (autoparachute == true) then
				GiveWeaponToPed(GetPlayerPed(-1), GetHashKey("gadget_parachute"), 1, false, false)
			end
		end
	end
end)

Citizen.CreateThread(function() --Blackout♀
	while true do
		Citizen.Wait(0)

		if (blackout == true) then
			SetBlackout(true)
		else
			SetBlackout(false)
		end
	end
end)

Citizen.CreateThread(function() --PvP Enabled
	while true do
		Citizen.Wait(0)
		SetCanAttackFriendly(GetPlayerPed(-1), true, false)
		NetworkSetFriendlyFireOption(true)
	end
end)

Citizen.CreateThread(function() --Mobile Radio
	while true do
		Citizen.Wait(0)

		if GetPlayerPed(-1) then
			SetMobileRadioEnabledDuringGameplay(mobileradio)
		end
	end
end)

Citizen.CreateThread(function() --Count FPS (Thanks To siggyfawn [forum.FiveM.net])
	while not NetworkIsPlayerActive(PlayerId()) or not NetworkIsSessionStarted() do
		Citizen.Wait(0)
		prevframes = GetFrameCount()
		prevtime = GetGameTimer()
	end

	while true do
		curtime = GetGameTimer()
		curframes = GetFrameCount()

	    if((curtime - prevtime) > 1000) then
			fps = (curframes - prevframes) - 1				
			prevtime = curtime
			prevframes = curframes
	    end
		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function() --Draw FPS
    while true do
        Citizen.Wait(0)
		if (dfps == true) then
			if fps == 0 then
				Draw("FPS Count Failed", 255, 0, 0, 0.95, 0.97, 0.35, 0.35, 1, true)
				_DrawRect(0.950, 0.984, 0.0938, 0.025, 0, 0, 0, 127, 0)
			elseif fps >= 1 and fps <= 30 then
				Draw("" .. fps .. "", 255, 0, 0, 0.99, 0.97, 0.35, 0.35, 1, true)
				_DrawRect(0.99, 0.984, 0.0175, 0.025, 0, 0, 0, 127, 0)
			elseif fps >=31 and fps <= 50 then
				Draw("" .. fps .. "", 255, 255, 0, 0.99, 0.97, 0.35, 0.35, 1, true)
				_DrawRect(0.99, 0.984, 0.0175, 0.025, 0, 0, 0, 127, 0)
			elseif fps >= 51 then
				Draw("" .. fps .. "", 0, 255, 0, 0.99, 0.97, 0.35, 0.35, 1, true)
				_DrawRect(0.99, 0.984, 0.0175, 0.025, 0, 0, 0, 127, 0)
			end
		end
	end
end)

Citizen.CreateThread(function() --Draw Speedometer
    while true do
        Citizen.Wait(0)
		if (dspeedo == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				local cleanspeed = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false))
				_DrawRect(0.205, 0.964, 0.07, 0.04, 0, 0, 0, 127, 0)
				if kmh == true then
					Draw("" .. math.ceil(cleanspeed * 3.6) .. " KMH", 255, 255, 255, 0.205, 0.95, 0.4, 0.4, 1, true)
				elseif mph == true then
					Draw("" .. math.ceil(cleanspeed * 2.236936) .. " MPH", 255, 255, 255, 0.205, 0.95, 0.4, 0.4, 1, true)
				end
			end
		end
	end
end)

Citizen.CreateThread(function() --No NPCs & Traffic
	while true do
		Citizen.Wait(0)
		if (nonpcstraffic == true) then
			local playerPedPos = GetEntityCoords(GetPlayerPed(-1)) 
			SetPedDensityMultiplierThisFrame(0.0)
			SetScenarioPedDensityMultiplierThisFrame(0.0, 0.0)
			SetVehicleDensityMultiplierThisFrame(0.0)
			SetRandomVehicleDensityMultiplierThisFrame(0.0)
			SetParkedVehicleDensityMultiplierThisFrame(0.0)
			Citizen.InvokeNative(0x90B6DA738A9A25DA, 0.0)
			RemoveVehiclesFromGeneratorsInArea(playerPedPos.x - 1000.0, playerPedPos.y - 1000.0, playerPedPos.z - 1000.0, playerPedPos.x + 1000.0, playerPedPos.y + 1000.0, playerPedPos.z + 1000.0)
			SetGarbageTrucks(0)
			SetRandomBoats(0)
		end
	end
end)

Citizen.CreateThread(function() --Explode Nearest Vehicle
	while true do
		Citizen.Wait(0)
		if (explodevehicles == true) then
			local playerPedPos = GetEntityCoords(GetPlayerPed(-1), true)
			local NearestVehicle = GetClosestVehicle(playerPedPos, 250.0, 0, 4)
			NetworkExplodeVehicle(NearestVehicle, true, true, false)
		end
	end
end)

Citizen.CreateThread(function() --No Clip Mode (Miscellaneous)
	while true do
		Citizen.Wait(0)

		if (noclipmode == true) then
			SetEntityCollision(GetPlayerPed(-1), false, false)
			ClearPedTasksImmediately(GetPlayerPed(-1))
			StopAllScreenEffects()
			HideHudComponentThisFrame(2)
			HideHudComponentThisFrame(19)
			HideHudComponentThisFrame(20)
			HideHudComponentThisFrame(22)
			Citizen.InvokeNative(0x0AFC4AF510774B47)
			if not IsControlPressed(1, 32) and not IsControlPressed(1, 33) and not IsControlPressed(1, 90) and not IsControlPressed(1, 89) then
				FreezeEntityPosition(GetPlayerPed(-1), true)
			end
		end
	end
end)

Citizen.CreateThread(function() --No Clip Mode (Forward/ Backward)
	while true do
		Citizen.Wait(0)

		if (noclipmode == true) then
			local coords = Citizen.InvokeNative(0x0A794A5A57F8DF91, PlayerPedId(), Citizen.ResultAsVector())
			if IsControlPressed(1, 32) then --Forward
				FreezeEntityPosition(GetPlayerPed(-1), false)
				ApplyForceToEntity(GetPlayerPed(-1), 1, coords.x * 3, coords.y * 3, 0.27, 0.0, 0.0, 0.0, 1, false, true, true, true, true)
			elseif IsControlPressed(1, 33) then --Backward
				FreezeEntityPosition(GetPlayerPed(-1), false)
				ApplyForceToEntity(GetPlayerPed(-1), 1, coords.x * -3, coords.y * -3, 0.27, 0.0, 0.0, 0.0, 1, false, true, true, true, true)
			end
		end
	end
end)

Citizen.CreateThread(function() --No Clip Mode (Up/ Down)
	while true do
		Citizen.Wait(0)

		if (noclipmode == true) then
			if IsControlPressed(1, 90) then --Up
				FreezeEntityPosition(GetPlayerPed(-1), false)
				ApplyForceToEntity(GetPlayerPed(-1), 1, 0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 1, false, true, true, true, true)
			elseif IsControlPressed(1, 89) then --Down
				FreezeEntityPosition(GetPlayerPed(-1), false)
				ApplyForceToEntity(GetPlayerPed(-1), 1, 0.0, 0.0, -5.0, 0.0, 0.0, 0.0, 1, false, true, true, true, true)
			end
		end
	end
end)

Citizen.CreateThread(function() --No Clip Mode (Rotation)
	while true do
		Citizen.Wait(0)
		local camRot = GetGameplayCamRot(0)
		if (noclipmode == true) then
			SetEntityRotation(GetPlayerPed(-1), 0.0, 0.0, camRot.z, 1, true)
		end
	end
end)

Citizen.CreateThread(function() --Cinematic Cam Disabled
	while true do
		Citizen.Wait(0)
		if nonocinecam == true then
			SetCinematicButtonActive(false)
		else
			SetCinematicButtonActive(true)
		end
	end
end)

Citizen.CreateThread(function() --Freeze Radio Station
	while true do
		Citizen.Wait(0)
		if (freezeradio == true) then
			if (GetPlayerRadioStationName() ~= radioname) then
				SetRadioToStationName(radioname)
				SetVehRadioStation(GetVehiclePedIsIn(GetPlayerPed(-1), false), radioname)
			end
		end
	end
end)

Citizen.CreateThread(function() --Bodyguard Teleport
	while true do
		Citizen.Wait(0)
		local playerPedPos = GetEntityCoords(GetPlayerPed(-1), true)
		local GroupHandle = GetPlayerGroup(PlayerId())
		for i = 0, 6 do
			local ped = GetPedAsGroupMember(GroupHandle, i)
			local PedPos = GetEntityCoords(ped, true)
			if (Vdist(playerPedPos.x, playerPedPos.y, playerPedPos.z, PedPos.x, PedPos.y, PedPos.z) >= 50) then
				SetEntityCoords(ped, playerPedPos.x, playerPedPos.y, playerPedPos.z, false, false, false, true)
				SetEntityVisible(ped, true, 0)
			end
		end
	end
end)

Citizen.CreateThread(function() --Vibrator
	while true do
		Citizen.Wait(0)

		if (vibrator == true) then
			SetPadShake(0, 999999999, 255)
			vibcount = 1
		elseif (vibrator == false) and (vibcount == 1)then
			StopPadShake(0)
			vibcount = 0
		end
	end
end)

Citizen.CreateThread(function() --Freeze Current Time
	while true do
		Citizen.Wait(0)
		if (GetPlayerPed(-1) ~= 0) then
			PauseClock(freezecurrenttime)
		end
	end
end)

function CustomDraw(text, x, y) --Name Drawing
	SetTextFont(6)
	SetTextScale(0.36, 0.36)
	SetTextWrap(0.0, 1.0)
	SetTextCentre(false)
	SetTextDropshadow(0, 0, 0, 0, 0)
	SetTextEdge(1, 0, 0, 0, 205)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x, y)
end

function Draw(text, r, g, b, x, y, width, height, layer, center) --Funtion To Draw Strings
	SetTextColour(r, g, b, 127)
	SetTextFont(0)
	SetTextScale(width, height)
	SetTextWrap(0.0, 1.0)
	SetTextCentre(center)
	SetTextDropshadow(0, 0, 0, 0, 0)
	SetTextEdge(1, 0, 0, 0, 205)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	Citizen.InvokeNative(0x61BB1D9B3A95D802, layer)
	DrawText(x, y)
end

function _DrawRect(x, y, width, height, r, g, b, a, layer) --Function To Draw A Rect
	Citizen.InvokeNative(0x61BB1D9B3A95D802, layer)
	DrawRect(x, y, width, height, r, g, b, a)
end

function FirstToUpper(str) --Function To Turn First Letter in String To Upper
    return (str:gsub("^%l", string.upper))
end